# Introduction

This is a native iOS project for **Cocorian** ecommerce app. Te applecation is developed using Clean Architecture and MVVM. more on that later. please see the requerments for this project [here](./Requerments.md)

![Platforms](https://img.shields.io/badge/Platforms-iOS-lightgray.svg?logo=apple)
![Xcode Version](https://img.shields.io/badge/Xcode-12.0-blue.svg?logo=Xcode)
![Swift Version](https://img.shields.io/badge/Swift-5.3-orange.svg?logo=swift)
[![CocoaPods](https://img.shields.io/badge/CocoaPods-Available-EE3322.svg?logo=cocoapods)](https://cocoapods.org/)


# Requirements
- Swift 5.3
- Xcode 12.0 and above
- macOS 11.0 and above
- [Pod](https://cocoapods.org/) v1.6.0 and above - A package manager
- [Swiftlint](https://github.com/realm/SwiftLint) - A Swift code linter & fixer

# Getting started

You'll need a working MacOS development environment with XCode 12 to work on this
template. You can find instructions to get up and running on the Apple [XCode website](https://developer.apple.com/xcode/).

install cocoapods
```bash
gem install cocoapods
```
> you might need `sudo` to install.

Finally open `Chocorian.xcworkspace` and start development.

# Understanding Architecture

## Layers
* **Domain Layer** = Entities + Use Cases + Repositories Interfaces
* **Data Repositories Layer** = Repositories Implementations + API (Network) + Persistence DB
* **Presentation Layer (MVVM)** = ViewModels + Views

### Dependency Direction

![Alt text](https://miro.medium.com/max/5900/1*N3ypUNMUGv87qUL57JyqJA.png)

**Note:** **Domain Layer** should not include anything from other layers(e.g Presentation — UIKit or SwiftUI or Data Layer — Mapping Codable)

For more information please read [this medium post](https://tech.olx.com/clean-architecture-and-mvvm-on-ios-c9d167d9f5b3) about Clean Architecture + MVVM.