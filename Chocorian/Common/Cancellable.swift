//
//  Cancellable.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/24/21.
//

import Foundation

public protocol Cancellable {
    func cancel()
}
