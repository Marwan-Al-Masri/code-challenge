//
//  AppAppearance.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/24/21.
//

import Foundation
import UIKit

final class AppAppearance {
    static func setupAppearance() {
        let accent = UIColor(red: 0.24, green: 0.42, blue: 1.00, alpha: 1.00)
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = accent
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: accent]
        UITabBar.appearance().tintColor = accent
    }
}

extension UINavigationController {
    @objc override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
