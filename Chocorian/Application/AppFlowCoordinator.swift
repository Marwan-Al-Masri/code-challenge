//
//  AppFlowCoordinator.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/24/21.
//

import UIKit

final class AppFlowCoordinator {

    var navigationController: UINavigationController
    private let appDIContainer: AppDIContainer
    
    init(navigationController: UINavigationController,
         appDIContainer: AppDIContainer) {
        self.navigationController = navigationController
        self.appDIContainer = appDIContainer
    }

    func start() {
        if appDIContainer.appConfiguration.apiToken == nil {
            let loginSceneDIContainer = appDIContainer.makeLoginSceneDIContainer()
            let flow = loginSceneDIContainer.makeAuthenticationFlowCoordinator(navigationController: navigationController)
            flow.start()
        } else {
            let mainSceneDIContainer = appDIContainer.makeMainSceneDIContainer()
            let flow = mainSceneDIContainer.makeMainFlowCoordinator(navigationController: navigationController)
            flow.start()
        }
    }
}
