//
//  AppConfiguration.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/24/21.
//

import Foundation

protocol AppConfiguration {
    var apiBaseURL: String { get }
    var apiToken: String? { get  set }
}

final class DefaultAppConfiguration: AppConfiguration {
    lazy var apiBaseURL: String = {
        guard let apiBaseURL = Bundle.main.object(forInfoDictionaryKey: "ApiBaseURL") as? String else {
            fatalError("ApiBaseURL must not be empty in info.plist")
        }
        return apiBaseURL
    }()
    
    var apiToken: String? {
        get {
            return UserDefaults.standard.object(forKey: "USER_AUTH") as? String
        }
        set {
            if let newValue = newValue {
                UserDefaults.standard.set(newValue, forKey: "USER_AUTH")
            } else {
                UserDefaults.standard.removeObject(forKey: "USER_AUTH")
            }
        }
    }

}
