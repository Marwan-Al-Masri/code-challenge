//
//  MainSceneDIContainer.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import UIKit
import SwiftUI

final class MainSceneDIContainer {
    
    struct Dependencies {
        let apiDataTransferService: DataTransferService
        let appConfiguration: AppConfiguration
    }
    
    private let dependencies: Dependencies
    
    // MARK: - Persistent Storage
    lazy var cartResponseCache: CartResponseStorage = CartUserDefaultStorage()
    lazy var orderResponseCache: OrderResponseStorage = CoreDataOrderResponseStorage()

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    // MARK: - Use Cases
    func makeFetchProductsUseCase() -> FetchProductsUseCase {
        return DefaultFetchProductsUseCase(token: dependencies.appConfiguration.apiToken ?? "", procudtsRepository: makeProductRepository())
    }
    
    func makeFetchCartUseCase() -> FetchCartUseCase {
        return DefaultFetchCartUseCase(cartRepository: makeCartRepository())
    }
    
    func makeSaveCartUseCase() -> SaveCartUseCase {
        return DefaultSaveCartUseCase(cartRepository: makeCartRepository())
    }
    
    func makeClearCartUseCase() -> ClearCartUseCase {
        return DefaultClearCartUseCase(cartRepository: makeCartRepository())
    }
    
    func makeFetchOrdersUseCase() -> FetchOrdersUseCase {
        return DefaultFetchOrdersUseCase(orderRepository: makeOrderRepository())
    }
    
    func makeSaveOrderUseCase() -> SaveOrderUseCase {
        return DefaultSaveOrderUseCase(orderRepository: makeOrderRepository())
    }
    
    func makeClearOrdersUseCase() -> ClearOrdersUseCase {
        return DefaultClearOrdersUseCase(orderRepository: makeOrderRepository())
    }
    
    // MARK: - Repositories
    func makeProductRepository() -> ProductRepository {
        return DefaultProductRepository(dataTransferService: dependencies.apiDataTransferService)
    }
    
    func makeCartRepository() -> CartRepository {
        return DefaultCartRepository(cache: cartResponseCache)
    }
    
    func makeOrderRepository() -> OrderRepository {
        return DefaultOrderRepository(cache: orderResponseCache)
    }
    
    // MARK: - Tab Bar View
    func makeTabBarController(actions: TabBarViewModelActions) -> TabBarController {
        return TabBarController.create(with: makeTabBarViewModel(actions: actions))
    }
    
    func makeTabBarViewModel(actions: TabBarViewModelActions) -> TabBarViewModel {
        return DefaultTabBarViewModel(actions: actions)
    }
    
    // MARK: - Products List View
    func makeProductsListController() -> ProductsListViewController {
        return ProductsListViewController.create(with: makeProductsListViewModel())
    }
    
    func makeProductsListViewModel() -> ProductsListViewModel {
        return DefaultProductsListViewModel(fetchProductsUseCase: makeFetchProductsUseCase(),
                                            fetchCartUseCase: makeFetchCartUseCase(),
                                            saveCartUseCase: makeSaveCartUseCase(),
                                            appConfiguration: dependencies.appConfiguration)
    }
    
    // MARK: - Cart List View
    func makeCartListViewController() -> CartListViewController {
        return CartListViewController.create(with: makeCartListViewModel())
    }
    
    func makeCartListViewModel() -> CartListViewModel {
        return DefaultCartListViewModel(fetchCartUseCase: makeFetchCartUseCase(),
                                        saveCartUseCase: makeSaveCartUseCase(),
                                        clearCartUseCase: makeClearCartUseCase(),
                                        saveOrderUseCase: makeSaveOrderUseCase(),
                                        appConfiguration: dependencies.appConfiguration)
    }
    
    // MARK: - Orders List View
    func makeOrdersListController(actions: OrdersListViewModelActions) -> OrdersListViewController {
        return OrdersListViewController.create(with: makeOrdersListViewModel(actions: actions))
    }
    
    func makeOrdersListViewModel(actions: OrdersListViewModelActions) -> OrdersListViewModel {
        return DefaultOrdersListViewModel(fetchOrdersUseCase: makeFetchOrdersUseCase(),
                                          actions: actions)
    }
    
    func makeOrderDetailsController(order: Order) -> OrderDetailsViewController {
        return OrderDetailsViewController.create(with: makeOrderDetailsViewModel(order: order))
    }
    
    func makeOrderDetailsViewModel(order: Order) -> OrderDetailsViewModel {
        return DefaultOrderDetailsViewModel(order: order)
    }
    
    // MARK: - Settings View
    func makeSettingsController() -> SettingsViewController {
        return SettingsViewController.create(with: makeSettingsViewModel())
    }
    
    func makeSettingsViewModel() -> SettingsViewModel {
        return DefaultSettingsViewModel(clearCartUseCase: makeClearCartUseCase(),
                                        clearOrdersUseCase: makeClearOrdersUseCase(),
                                        appConfiguration: dependencies.appConfiguration)
    }
    
    // MARK: - Flow Coordinators
    func makeMainFlowCoordinator(navigationController: UINavigationController) -> MainFlowCoordinator {
        return MainFlowCoordinator(navigationController: navigationController,
                                   dependencies: self)
    }
}

extension MainSceneDIContainer: MainFlowCoordinatorDependencies { }
