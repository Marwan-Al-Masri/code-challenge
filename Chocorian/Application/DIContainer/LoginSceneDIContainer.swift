//
//  LoginSceneDIContainer.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import UIKit
import SwiftUI

final class LoginSceneDIContainer {
    
    struct Dependencies {
        let apiDataTransferService: DataTransferService
        let appConfiguration: AppConfiguration
    }
    
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    // MARK: - Use Cases
    func makeAuthenticationUseCase() -> AuthenticationUseCase {
        return DefaultAuthenticationUseCase(authenticationRepository: makeAuthenticationRepository())
    }
    
    // MARK: - Repositories
    func makeAuthenticationRepository() -> AuthenticationRepository {
        return DefaultAuthenticationRepository(dataTransferService: dependencies.apiDataTransferService)
    }
    
    // MARK: - Login View
    func makeLoginViewController() -> LoginViewController {
        return LoginViewController.create(with: makeLoginViewModel())
    }
    
    func makeLoginViewModel() -> LoginViewModel {
        return DefaultLoginViewModel(authenticationUseCase: makeAuthenticationUseCase(),
                                     appConfiguration: dependencies.appConfiguration)
    }
    
    // MARK: - Flow Coordinators
    func makeAuthenticationFlowCoordinator(navigationController: UINavigationController) -> AuthenticationFlowCoordinator {
        return AuthenticationFlowCoordinator(navigationController: navigationController,
                                             dependencies: self)
    }
}

extension LoginSceneDIContainer: AuthenticationFlowCoordinatorDependencies { }
