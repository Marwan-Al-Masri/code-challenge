//
//  AppDIContainer.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/24/21.
//

import Foundation

final class AppDIContainer {
    
    var appConfiguration: AppConfiguration
    
    // MARK: - Network
    lazy var apiDataTransferService: DataTransferService = {
        let config = ApiDataNetworkConfig(baseURL: URL(string: appConfiguration.apiBaseURL)!,
                                          queryParameters: [:])

        let apiDataNetwork = DefaultNetworkService(config: config)
        return DefaultDataTransferService(with: apiDataNetwork)
    }()
    
    init(appConfiguration: AppConfiguration = DefaultAppConfiguration()) {
        self.appConfiguration = appConfiguration
    }

    // MARK: - DIContainers of scenes
    func makeLoginSceneDIContainer() -> LoginSceneDIContainer {
        let dependencies = LoginSceneDIContainer.Dependencies(apiDataTransferService: apiDataTransferService, appConfiguration: appConfiguration)
        return LoginSceneDIContainer(dependencies: dependencies)
    }
    
    func makeMainSceneDIContainer() -> MainSceneDIContainer {
        let dependencies = MainSceneDIContainer.Dependencies(apiDataTransferService: apiDataTransferService, appConfiguration: appConfiguration)
        return MainSceneDIContainer(dependencies: dependencies)
    }
}
