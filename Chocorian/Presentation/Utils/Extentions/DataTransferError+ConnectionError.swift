//
//  DataTransferError+ConnectionError.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import Foundation

extension DataTransferError: ConnectionError {
    public var isInternetConnectionError: Bool {
        guard case let DataTransferError.networkFailure(networkError) = self,
            case .notConnected = networkError else {
                return false
        }
        return true
    }
}
