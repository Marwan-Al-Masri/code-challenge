//
//  LoginViewController.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import UIKit

class LoginViewController: UIViewController, StoryboardInstantiable, Alertable {

    @IBOutlet var textFieldEmail: UITextField!
    @IBOutlet var textFieldPassword: UITextField!
    @IBOutlet var buttonHideShowPassword: UIButton!
    
    private var viewModel: LoginViewModel!
    
    // MARK: - Lifecycle

    static func create(with viewModel: LoginViewModel) -> LoginViewController {
        let view = LoginViewController.instantiateViewController()
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = true
        textFieldEmail.setLeftPadding(value: 15)
        textFieldPassword.setLeftPadding(value: 15)
        textFieldPassword.setRightPadding(value: 40)
        bind(to: viewModel)
    }
    
    private func bind(to viewModel: LoginViewModel) {
        viewModel.error.observe(on: self) { self.showError($0) }
        viewModel.loading.observe(on: self) { self.updateLoading($0) }
    }

    // MARK: - User actions
    @IBAction func actionHideShowPassword(_ sender: Any) {
        buttonHideShowPassword.isSelected = !buttonHideShowPassword.isSelected
        textFieldPassword.isSecureTextEntry = !buttonHideShowPassword.isSelected
    }

    @IBAction func actionLogin(_ sender: Any) {
        guard let email = textFieldEmail.text, !email.isEmpty,
              let password = textFieldPassword.text, !password.isEmpty
        else {
            showError("Email or Passowrd filed couldn't be empty")
            return
        }
        viewModel.login(email: email, password: password)
    }
    
    private func showError(_ error: String) {
        guard !error.isEmpty else { return }
        showAlert(title: "Error", message: error)
    }
    
    private func updateLoading(_ loading: Bool) {
        loading ? LoadingView.show() : LoadingView.hide()
    }
}
