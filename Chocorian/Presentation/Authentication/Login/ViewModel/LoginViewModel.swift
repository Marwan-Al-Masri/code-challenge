//
//  LoginViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import Foundation
import UIKit

protocol LoginViewModelInput {
    func login(email: String, password: String)
}

protocol LoginViewModelOutput {
    var error: Observable<String> { get }
    var loading: Observable<Bool> { get }
}

protocol LoginViewModel: LoginViewModelInput, LoginViewModelOutput { }

final class DefaultLoginViewModel: LoginViewModel {
    private let authenticationUseCase: AuthenticationUseCase
    private var appConfiguration: AppConfiguration
    
    var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    var authentication: Authentication?
    private var loginTask: Cancellable? { willSet { loginTask?.cancel() } }
    
    // MARK: - OUTPUT
    
    let error: Observable<String> = Observable("")
    let loading: Observable<Bool> = Observable(false)
    
    init(authenticationUseCase: AuthenticationUseCase, appConfiguration: AppConfiguration) {
        self.authenticationUseCase = authenticationUseCase
        self.appConfiguration = appConfiguration
    }
}

// MARK: - INPUT. View event methods
extension DefaultLoginViewModel {
    
    func login(email: String, password: String) {
        self.loading.value = true
        loginTask = self.authenticationUseCase.execute(requestValue: .init(email: email, password: password)) { result in
            self.loading.value = false
            switch result {
            case.success(let authentication):
                self.authentication = authentication
                self.appConfiguration.apiToken = authentication.token
                self.appDelegate?.appFlowCoordinator?.start()
            case.failure(let error):
                self.error.value = "Wrong Email or Passowrd"
                print(error)
            }
        }
    }
}
