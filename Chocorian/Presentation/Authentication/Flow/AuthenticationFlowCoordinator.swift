//
//  AuthenticationFlowCoordinator.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import UIKit

protocol AuthenticationFlowCoordinatorDependencies {
    func makeLoginViewController() -> LoginViewController
}

final class AuthenticationFlowCoordinator {
    
    private weak var navigationController: UINavigationController?
    private let dependencies: AuthenticationFlowCoordinatorDependencies

    private weak var loginVC: LoginViewController?

    init(navigationController: UINavigationController,
         dependencies: AuthenticationFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    func start() {
        // Note: here we keep strong reference with actions, this way this flow do not need to be strong referenced
        let vc = dependencies.makeLoginViewController()

        navigationController?.pushViewController(vc, animated: false)
        loginVC = vc
    }
}
