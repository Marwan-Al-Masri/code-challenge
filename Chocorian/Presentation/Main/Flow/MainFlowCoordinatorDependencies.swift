//
//  MainFlowCoordinatorDependencies.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import UIKit

protocol MainFlowCoordinatorDependencies {
    func makeTabBarController(actions: TabBarViewModelActions) -> TabBarController
    func makeProductsListController() -> ProductsListViewController
    func makeSettingsController() -> SettingsViewController
    func makeCartListViewController() -> CartListViewController
    func makeOrdersListController(actions: OrdersListViewModelActions) -> OrdersListViewController
    func makeOrderDetailsController(order: Order) -> OrderDetailsViewController
}

final class MainFlowCoordinator {
    
    private weak var navigationController: UINavigationController?
    private let dependencies: MainFlowCoordinatorDependencies
    private weak var productsListVC: ProductsListViewController?
    private weak var settingsVC: SettingsViewController?

    init(navigationController: UINavigationController,
         dependencies: MainFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    func start() {
        let productsListVC = dependencies.makeProductsListController()
        let ordersListActions = OrdersListViewModelActions(showOrderDetails: showOrderDetails)
        let ordersListVC = dependencies.makeOrdersListController(actions: ordersListActions)
        let settingsVC = dependencies.makeSettingsController()
        let tabBarActions = TabBarViewModelActions(showCart: showCart)
        
        let tabBarController = dependencies.makeTabBarController(actions: tabBarActions)
        tabBarController.setViewControllers([productsListVC, ordersListVC, settingsVC], animated: true)
        
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setViewControllers([tabBarController], animated: true)
        
        self.productsListVC = productsListVC
        self.settingsVC = settingsVC
    }
    
    private func showCart() {
        let vc = dependencies.makeCartListViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func showOrderDetails(_ order: Order) {
        let vc =  dependencies.makeOrderDetailsController(order: order)
        navigationController?.pushViewController(vc, animated: true)
    }
}
