//
//  TabBarViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

struct TabBarViewModelActions {
    let showCart: () -> Void
}

protocol TabBarViewModelInput {
    func showCart()
}

protocol TabBarViewModelOutput {
}

protocol TabBarViewModel: TabBarViewModelInput, TabBarViewModelOutput {}

final class DefaultTabBarViewModel: TabBarViewModel {

    private let actions: TabBarViewModelActions?

    // MARK: - OUTPUT

    // MARK: - Init

    init(actions: TabBarViewModelActions? = nil) {
        self.actions = actions
    }
}

// MARK: - INPUT. View event methods

extension DefaultTabBarViewModel {

    func showCart() {
        actions?.showCart()
    }
}
