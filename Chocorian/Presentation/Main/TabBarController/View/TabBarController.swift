//
//  TabBarController.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import UIKit

final class TabBarController: UITabBarController {
    
    private var cartButton: UIBarButtonItem?

    // MARK: - Lifecycle
    
    private var viewModel: TabBarViewModel!

    static func create(with viewModel: TabBarViewModel) -> TabBarController {
        let view = TabBarController()
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        
        self.cartButton = UIBarButtonItem(image: UIImage(systemName: "cart.fill"),
                                          style: .plain,
                                          target: self,
                                          action: #selector(showCart(_:)))
        
        self.navigationItem.rightBarButtonItem = cartButton
    }
    
    @objc func showCart(_ sender: Any) {
        viewModel.showCart()
    }
}
