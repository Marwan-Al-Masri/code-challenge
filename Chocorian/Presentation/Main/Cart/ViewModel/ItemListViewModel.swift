//
//  ItemListViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

struct ItemListViewModel: Equatable {
    let id: String
    let name: String
    let description: String
    let price: Double
    let photoPath: String?
    var quantity = 0
}

extension ItemListViewModel {

    init(product: Product, quantity: Int) {
        self.id = product.id
        self.name = product.name
        self.description = product.productDescription
        self.price = product.price
        self.photoPath = product.photo
        self.quantity = quantity
    }
    
    func toDomain() -> Item {
        let product = Product(id: id, name: name, productDescription: description, price: price, photo: photoPath)
        return .init(product: product, quantity: quantity)
    }
}
