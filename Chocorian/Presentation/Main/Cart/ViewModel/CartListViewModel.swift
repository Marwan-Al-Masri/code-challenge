//
//  CartListViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

protocol CartListViewModelInput {
    func viewDidLoad()
    func load()
    func update(item: ItemListViewModel, at index: Int)
    func remove(at index: Int)
    func clear()
    func PlaceOrder(completion: @escaping (Result<Void, Error>) -> Void)
}

protocol CartListViewModelOutput {
    var items: Observable<[ItemListViewModel]> { get }
    var loading: Observable<Bool> { get }
    var error: Observable<String> { get }
}

protocol CartListViewModel: CartListViewModelInput, CartListViewModelOutput {}

final class DefaultCartListViewModel: CartListViewModel {

    private let fetchCartUseCase: FetchCartUseCase
    private let saveCartUseCase: SaveCartUseCase
    private let clearCartUseCase: ClearCartUseCase
    private let saveOrderUseCase: SaveOrderUseCase
    private var appConfiguration: AppConfiguration

    // MARK: - OUTPUT

    let items: Observable<[ItemListViewModel]> = Observable([])
    let loading: Observable<Bool> = Observable(false)
    let error: Observable<String> = Observable("")

    // MARK: - Init

    init(fetchCartUseCase: FetchCartUseCase,
         saveCartUseCase: SaveCartUseCase,
         clearCartUseCase: ClearCartUseCase,
         saveOrderUseCase: SaveOrderUseCase,
         appConfiguration: AppConfiguration) {
        self.fetchCartUseCase = fetchCartUseCase
        self.saveCartUseCase = saveCartUseCase
        self.clearCartUseCase = clearCartUseCase
        self.saveOrderUseCase = saveOrderUseCase
        self.appConfiguration = appConfiguration
    }

    // MARK: - Private

    private func handle(error: Error) {
        self.error.value = NSLocalizedString("Failed loading cart", comment: "")
    }
}

// MARK: - INPUT. View event methods

extension DefaultCartListViewModel {

    func viewDidLoad() {
        self.load()
    }
    
    func update(item: ItemListViewModel, at index: Int) {
        items.value[index] = item
        update()
    }
    
    func remove(at index: Int) {
        items.value.remove(at: index)
        update()
    }
    
    func load() {
        self.loading.value = true
        fetchCartUseCase.execute(completion: { result in
            self.loading.value = false
            switch result {
            case .success(let list):
                self.items.value = list.map { ItemListViewModel(product: $0.product, quantity: $0.quantity) }
            case .failure(let error):
                self.handle(error: error)
            }
        })
    }
    
    func update() {
        let cart = self.items.value.map { $0.toDomain() }
        saveCartUseCase.execute(cart: cart, completion: { result in
            switch result {
            case .success:
                break
            case .failure(let error):
                self.handle(error: error)
            }
        })
    }

    func clear() {
        clearCartUseCase.execute(completion: { result in
            switch result {
            case .success:
                self.items.value = []
            case .failure(let error):
                self.handle(error: error)
            }
        })
    }
    
    func PlaceOrder(completion: @escaping (Result<Void, Error>) -> Void) {
        let cart = self.items.value.map { $0.toDomain() }
        let order = Order(id: UUID().uuidString, createDate: Date(), items: cart)
        saveOrderUseCase.execute(order: order, completion: { result in
            switch result {
            case .success:
                self.clear()
                completion(.success(()))
            case .failure(let error):
                self.error.value = "Coudn't save your order!"
                completion(.failure(error))
            }
        })
    }
}
