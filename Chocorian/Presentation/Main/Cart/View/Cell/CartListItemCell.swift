//
//  CartListItemCell.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import UIKit
import Kingfisher

final class CartListItemCell: UITableViewCell {

    static let reuseIdentifier = String(describing: CartListItemCell.self)
    static let height = CGFloat(130)
    
    @IBOutlet var imageProduct: UIImageView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelDscription: UILabel!
    @IBOutlet var labelPrice: UILabel!
    @IBOutlet var labelItems: UILabel!
    @IBOutlet var buttonAddItem: UIButton!
    @IBOutlet var buttonRemoveItem: UIButton!
    @IBOutlet var viewCart: UIView!
    @IBOutlet var buttonCart: UIButton!

    private var viewModel: ItemListViewModel!
    
    private var indexPath: IndexPath?
    
    private var updateHandler: ((ItemListViewModel) -> Void)?
    private var removeHandler: ((ItemListViewModel) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageProduct.layer.cornerRadius = 10
        viewCart.layer.borderWidth = 1
        viewCart.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageProduct?.image = nil
        labelTitle.text = nil
        labelDscription.text = nil
        labelPrice.text = nil
        labelItems.text = "0"
    }

    func fill(with viewModel: ItemListViewModel,
              at indexPath: IndexPath,
              onUpdate updateHandler: @escaping ((ItemListViewModel) -> Void),
              onRemove removeHandler: @escaping ((ItemListViewModel) -> Void)) {
        self.viewModel = viewModel
        self.indexPath = indexPath
        self.updateHandler = updateHandler
        self.removeHandler = removeHandler
        if let photoPath = viewModel.photoPath, let url = URL(string: photoPath) {
            imageProduct.kf.setImage(with: url, placeholder: UIImage(systemName: "photo.fill"))
        } else {
            imageProduct.image = UIImage(systemName: "photo.fill")
        }
        labelTitle.text = viewModel.name
        labelDscription.text = viewModel.description
        labelPrice.text = "$" + String(viewModel.price)
        updateQuantity()
    }
    
    @IBAction func actionAddItem(_ sender: UIButton) {
        viewModel.quantity += 1
        updateQuantity()
        self.updateHandler?(viewModel)
    }

    @IBAction func actionRemoveItem(_ sender: UIButton) {
        guard viewModel.quantity > 0 else { return }
        viewModel.quantity -= 1
        updateQuantity()
        self.updateHandler?(viewModel)
    }

    @IBAction func actionRemove(_ sender: UIButton) {
        self.removeHandler?(viewModel)
    }
    
    private func updateQuantity() {
        labelItems.text = String(viewModel.quantity)
    }
}
