//
//  CartListViewController.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import UIKit

final class CartListViewController: UIViewController, StoryboardInstantiable, Alertable {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var cartListContainer: UIView!
    @IBOutlet var labelItemCount: UILabel!
    @IBOutlet var labelSubTotal: UILabel!
    @IBOutlet var labelTotal: UILabel!
    
    private var viewModel: CartListViewModel!

    private var cartTableViewController: CartListTableViewController?

    // MARK: - Lifecycle

    static func create(with viewModel: CartListViewModel) -> CartListViewController {
        let view = CartListViewController.instantiateViewController()
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bind(to: viewModel)
        viewModel.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Cart"
    }

    private func bind(to viewModel: CartListViewModel) {
        viewModel.items.observe(on: self) { [weak self] _ in self?.updateItems() }
        viewModel.loading.observe(on: self) { [weak self] in self?.updateLoading($0) }
        viewModel.error.observe(on: self) { [weak self] in self?.showError($0) }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: CartListTableViewController.self),
            let destinationVC = segue.destination as? CartListTableViewController {
            cartTableViewController = destinationVC
            cartTableViewController?.viewModel = viewModel
        }
    }
    
    @IBAction func actionPlaceOrder(_ sender: UIButton) {
        viewModel.PlaceOrder { result in
            switch result {
            case .success:
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            case .failure:
                break
            }
        }
    }

    // MARK: - Private

    private func setupViews() {
        
        let cartButton = UIBarButtonItem(image: UIImage(systemName: "trash.fill"),
                                          style: .plain,
                                          target: self,
                                          action: #selector(clearCart(_:)))
        
        self.navigationItem.rightBarButtonItem = cartButton
    }

    private func updateItems() {
        cartTableViewController?.reload()
        let count = viewModel.items.value.reduce(0) { $0 + $1.quantity }
        let totalPrice = viewModel.items.value.reduce(0.0) { $0 + $1.price * Double($1.quantity) }
        labelItemCount.text = "\(count)" + " items"
        labelSubTotal.text = String(format: "Subtotal: $%.2f", totalPrice)
        labelTotal.text = String(format: "$%.2f", totalPrice)
    }

    private func updateLoading(_ loading: Bool) {
        loading ? LoadingView.show() : LoadingView.hide()
    }

    private func showError(_ error: String) {
        guard !error.isEmpty else { return }
        showAlert(title: "Error", message: error)
    }
    
    @objc func clearCart(_ sender: Any) {
        viewModel.clear()
    }
}
