//
//  CartListTableViewController.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import UIKit

final class CartListTableViewController: UITableViewController {

    var viewModel: CartListViewModel!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    func reload() {
        tableView.reloadData()
    }

    // MARK: - Private

    private func setupViews() {
        tableView.estimatedRowHeight = CartListItemCell.height
        tableView.rowHeight = UITableView.automaticDimension
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension CartListTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.value.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CartListItemCell.reuseIdentifier,
                                                       for: indexPath) as? CartListItemCell else {
            assertionFailure("Cannot dequeue reusable cell \(CartListItemCell.self) with reuseIdentifier: \(CartListItemCell.reuseIdentifier)")
            return UITableViewCell()
        }

        cell.fill(with: viewModel.items.value[indexPath.row], at: indexPath,
                  onUpdate: { item in
                    self.viewModel.update(item: item, at: indexPath.row)
                  },
                  onRemove: { _ in
                    self.viewModel.remove(at: indexPath.row)
                  })

        return cell
    }
}
