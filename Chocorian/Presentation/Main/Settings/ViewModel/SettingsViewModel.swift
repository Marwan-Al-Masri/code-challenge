//
//  SettingsViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation
import UIKit

protocol SettingsViewModelInput {
    func logout()
}

protocol SettingsViewModelOutput {
    var loading: Observable<Bool> { get }
    var error: Observable<String> { get }
}

protocol SettingsViewModel: SettingsViewModelInput, SettingsViewModelOutput {}

final class DefaultSettingsViewModel: SettingsViewModel {
    
    private let clearCartUseCase: ClearCartUseCase
    private let clearOrdersUseCase: ClearOrdersUseCase
    private var appConfiguration: AppConfiguration
    
    var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }

    // MARK: - OUTPUT

    let loading: Observable<Bool> = Observable(false)
    let error: Observable<String> = Observable("")

    // MARK: - Init

    init(clearCartUseCase: ClearCartUseCase,
         clearOrdersUseCase: ClearOrdersUseCase,
         appConfiguration: AppConfiguration) {
        self.clearCartUseCase = clearCartUseCase
        self.clearOrdersUseCase = clearOrdersUseCase
        self.appConfiguration = appConfiguration
    }
    
    func clearData() {
    }
}

// MARK: - INPUT. View event methods

extension DefaultSettingsViewModel {
    
    func logout() {
        loading.value = true
        self.clearCartUseCase.execute { result in
            switch result {
            case .success:
                self.clearOrdersUseCase.execute { result in
                    switch result {
                    case .success:
                        DispatchQueue.main.async {
                            self.appConfiguration.apiToken = nil
                            self.loading.value = false
                            self.appDelegate?.appFlowCoordinator?.start()
                        }
                    case .failure:
                        DispatchQueue.main.async {
                            self.loading.value = false
                            self.error.value = "Couldn't clear orders"
                        }
                    }
                }
            case .failure:
                DispatchQueue.main.async {
                    self.loading.value = false
                    self.error.value = "Couldn't clear cart"
                }
            }
        }
    }
}
