//
//  SettingsViewController.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import UIKit

final class SettingsViewController: UIViewController, StoryboardInstantiable, Alertable {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var logoutButton: UIButton!
    
    private var viewModel: SettingsViewModel!

    // MARK: - Lifecycle

    static func create(with viewModel: SettingsViewModel) -> SettingsViewController {
        let view = SettingsViewController.instantiateViewController()
        view.tabBarItem.image = UIImage(systemName: "gearshape")
        view.tabBarItem.selectedImage = UIImage(systemName: "gearshape.fill")
        view.tabBarItem.title = "Settings"
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bind(to: viewModel)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Settings"
    }

    private func bind(to viewModel: SettingsViewModel) {
        viewModel.loading.observe(on: self) { [weak self] in self?.updateLoading($0) }
        viewModel.error.observe(on: self) { [weak self] in self?.showError($0) }
    }
    
    // MARK: - User actions

    @IBAction func actionLogout(_ sender: Any) {
        
        let alert = UIAlertController(title: "Confermation",
                                      message: "Are you sure you want to logout?",
                                      preferredStyle: .alert)
        let changeAction = UIAlertAction(title: "Logout", style: .destructive, handler: { [weak self] _ in
            guard let self = self else { return }
            self.viewModel.logout()
        })
        alert.addAction(changeAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Private

    private func setupViews() {
    }

    private func updateLoading(_ loading: Bool) {
        loading ? LoadingView.show() : LoadingView.hide()
    }

    private func showError(_ error: String) {
        guard !error.isEmpty else { return }
        showAlert(title: "Error", message: error)
    }
}
