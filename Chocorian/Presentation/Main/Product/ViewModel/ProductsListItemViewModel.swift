//
//  ProductsListItemViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

struct ProductsListItemViewModel: Equatable {
    let id: String
    let name: String
    let description: String
    let price: Double
    let photoPath: String?
    var itemsCount = 0
}

extension ProductsListItemViewModel {

    init(product: Product) {
        self.id = product.id
        self.name = product.name
        self.description = product.productDescription
        self.price = product.price
        self.photoPath = product.photo
    }
    
    func toDomain() -> Product {
        return Product(id: id, name: name, productDescription: description, price: price, photo: photoPath)
    }
}
