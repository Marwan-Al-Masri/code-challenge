//
//  ProductsListViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

protocol ProductsListViewModelInput {
    func viewDidLoad()
    func load()
    func addToCart(item: ProductsListItemViewModel)
}

protocol ProductsListViewModelOutput {
    var items: Observable<[ProductsListItemViewModel]> { get }
    var loading: Observable<Bool> { get }
    var error: Observable<String> { get }
}

protocol ProductsListViewModel: ProductsListViewModelInput, ProductsListViewModelOutput {}

final class DefaultProductsListViewModel: ProductsListViewModel {

    private let fetchProductsUseCase: FetchProductsUseCase
    private let fetchCartUseCase: FetchCartUseCase
    private let saveCartUseCase: SaveCartUseCase
    private var appConfiguration: AppConfiguration

    private var products: [Product] = []
    private var productsLoadTask: Cancellable? { willSet { productsLoadTask?.cancel() } }

    // MARK: - OUTPUT

    let items: Observable<[ProductsListItemViewModel]> = Observable([])
    let loading: Observable<Bool> = Observable(false)
    let error: Observable<String> = Observable("")

    // MARK: - Init

    init(fetchProductsUseCase: FetchProductsUseCase,
         fetchCartUseCase: FetchCartUseCase,
         saveCartUseCase: SaveCartUseCase,
         appConfiguration: AppConfiguration) {
        self.fetchProductsUseCase = fetchProductsUseCase
        self.fetchCartUseCase = fetchCartUseCase
        self.saveCartUseCase = saveCartUseCase
        self.appConfiguration = appConfiguration
    }

    // MARK: - Private

    private func handle(error: Error) {
        self.error.value = error.isInternetConnectionError ?
            NSLocalizedString("No internet connection", comment: "") :
            NSLocalizedString("Failed loading product", comment: "")
    }
}

// MARK: - INPUT. View event methods

extension DefaultProductsListViewModel {

    func viewDidLoad() {
        self.load()
    }
    
    func load() {
        guard let token = appConfiguration.apiToken else {
            self.error.value = "Login is requered."
            return
        }
        self.loading.value = true
        productsLoadTask = fetchProductsUseCase.execute(token: token,
            completion: { result in
                self.loading.value = false
                switch result {
                case .success(let list):
                    self.items.value = list.map { ProductsListItemViewModel(product: $0) }
                case .failure(let error):
                    self.handle(error: error)
                }
        })
    }
    
    func addToCart(item: ProductsListItemViewModel) {
        let newItem = Item(product: item.toDomain(), quantity: item.itemsCount)
        fetchCartUseCase.execute { result in
            switch result {
            case .success(let list):
                var cart = list
                var found = false
                for object in cart.enumerated()
                where object.element.product.id == newItem.product.id {
                    if newItem.quantity == 0 {
                        cart.remove(at: object.offset)
                    } else {
                        cart[object.offset] = newItem
                    }
                    found = true
                }
                if !found {
                    cart.append(newItem)
                }
                self.saveCartUseCase.execute(cart: cart) { result in
                    switch result {
                    case .success: break
                    case .failure(let error):
                        print(error)
                        self.error.value = "Can't add to cart"
                    }
                }
            case .failure(let error):
                print(error)
                self.error.value = "Can't load cart"
            }
        }
    }
}
