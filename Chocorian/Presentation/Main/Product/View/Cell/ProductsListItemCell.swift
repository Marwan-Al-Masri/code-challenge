//
//  ProductsListItemCell.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import UIKit
import Kingfisher

final class ProductsListItemCell: UITableViewCell {

    static let reuseIdentifier = String(describing: ProductsListItemCell.self)
    static let height = CGFloat(130)
    
    @IBOutlet var imageProduct: UIImageView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelDscription: UILabel!
    @IBOutlet var labelPrice: UILabel!
    @IBOutlet var labelItems: UILabel!
    @IBOutlet var buttonAddItem: UIButton!
    @IBOutlet var buttonRemoveItem: UIButton!
    @IBOutlet var viewCart: UIView!
    @IBOutlet var buttonCart: UIButton!

    private var viewModel: ProductsListItemViewModel!
    
    private var indexPath: IndexPath?
    
    private var cartHandler: ((ProductsListItemViewModel) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageProduct.layer.cornerRadius = 12
        viewCart.layer.borderWidth = 1
        viewCart.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageProduct?.image = nil
        labelTitle.text = nil
        labelDscription.text = nil
        labelPrice.text = nil
        labelItems.text = "0"
        updateItemCount()
    }

    func fill(with viewModel: ProductsListItemViewModel,
              at indexPath: IndexPath,
              OnSetInCart cartHandler: @escaping ((ProductsListItemViewModel) -> Void)) {
        self.viewModel = viewModel
        self.indexPath = indexPath
        self.cartHandler = cartHandler
        if let photoPath = viewModel.photoPath, let url = URL(string: photoPath) {
            imageProduct.kf.setImage(with: url, placeholder: UIImage(systemName: "photo.fill"))
        } else {
            imageProduct.image = UIImage(systemName: "photo.fill")
        }
        labelTitle.text = viewModel.name
        labelDscription.text = viewModel.description
        labelPrice.text = "$" + String(viewModel.price)
        updateItemCount()
    }
    
    @IBAction func actionAddItem(_ sender: UIButton) {
        viewModel.itemsCount += 1
        updateItemCount()
    }

    @IBAction func actionRemoveItem(_ sender: UIButton) {
        guard viewModel.itemsCount > 0 else { return }
        viewModel.itemsCount -= 1
        updateItemCount()
    }

    @IBAction func actionCart(_ sender: UIButton) {
        self.cartHandler?(viewModel)
    }
    
    private func updateItemCount() {
        labelItems.text = String(viewModel.itemsCount)
    }
}
