//
//  ProductsListTableViewController.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import UIKit

final class ProductsListTableViewController: UITableViewController {

    var viewModel: ProductsListViewModel!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    func reload() {
        tableView.reloadData()
    }

    // MARK: - Private

    private func setupViews() {
        tableView.estimatedRowHeight = ProductsListItemCell.height
        tableView.rowHeight = UITableView.automaticDimension
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension ProductsListTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.value.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductsListItemCell.reuseIdentifier,
                                                       for: indexPath) as? ProductsListItemCell else {
            assertionFailure("Cannot dequeue reusable cell \(ProductsListItemCell.self) with reuseIdentifier: \(ProductsListItemCell.reuseIdentifier)")
            return UITableViewCell()
        }

        cell.fill(with: viewModel.items.value[indexPath.row], at: indexPath) { item in
            self.viewModel.addToCart(item: item)
        }

        return cell
    }
}
