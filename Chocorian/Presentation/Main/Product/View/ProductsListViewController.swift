//
//  ProductsListViewController.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import UIKit

final class ProductsListViewController: UIViewController, StoryboardInstantiable, Alertable {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var productsListContainer: UIView!
    
    private var viewModel: ProductsListViewModel!

    private var productsTableViewController: ProductsListTableViewController?

    // MARK: - Lifecycle

    static func create(with viewModel: ProductsListViewModel) -> ProductsListViewController {
        let view = ProductsListViewController.instantiateViewController()
        view.tabBarItem.image = UIImage(systemName: "bag")
        view.tabBarItem.selectedImage = UIImage(systemName: "bag.fill")
        view.tabBarItem.title = "Products"
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bind(to: viewModel)
        viewModel.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Products"
    }

    private func bind(to viewModel: ProductsListViewModel) {
        viewModel.items.observe(on: self) { [weak self] _ in self?.updateItems() }
        viewModel.loading.observe(on: self) { [weak self] in self?.updateLoading($0) }
        viewModel.error.observe(on: self) { [weak self] in self?.showError($0) }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: ProductsListTableViewController.self),
            let destinationVC = segue.destination as? ProductsListTableViewController {
            productsTableViewController = destinationVC
            productsTableViewController?.viewModel = viewModel
        }
    }

    // MARK: - Private

    private func setupViews() {
    }

    private func updateItems() {
        productsTableViewController?.reload()
    }

    private func updateLoading(_ loading: Bool) {
        loading ? LoadingView.show() : LoadingView.hide()
    }

    private func showError(_ error: String) {
        guard !error.isEmpty else { return }
        showAlert(title: "Error", message: error)
    }
}
