//
//  OrderDetailsItemCell.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import UIKit

class OrderDetailsItemCell: UITableViewCell {
    
    static let reuseIdentifier = String(describing: OrderDetailsItemCell.self)
    static let height = CGFloat(130)
    
    @IBOutlet var containerView: UIView!
    @IBOutlet var imageProduct: UIImageView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelPrice: UILabel!
    @IBOutlet var labelDscription: UILabel!
    @IBOutlet var labelQuantity: UILabel!
    
    private var viewModel: ItemListViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 12
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor.lightGray.cgColor
        containerView.layer.shadowColor = UIColor.gray.cgColor
        containerView.layer.shadowOffset = .init(width: -5, height: 5)
        containerView.layer.shadowRadius = 12
        containerView.layer.shadowOpacity = 0.5
        imageProduct.layer.cornerRadius = 10
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageProduct?.image = nil
        labelTitle.text = nil
        labelDscription.text = nil
        labelPrice.text = "$0"
        labelQuantity.text = "QTY: 0"
    }
    
    func fill(with viewModel: ItemListViewModel) {
        self.viewModel = viewModel
        if let photoPath = viewModel.photoPath, let url = URL(string: photoPath) {
            imageProduct.kf.setImage(with: url, placeholder: UIImage(named: "img_placeholder"))
        } else {
            imageProduct.image = UIImage(named: "img_placeholder")
        }
        labelTitle.text = viewModel.name
        labelPrice.text = "$" + String(viewModel.price)
        labelDscription.text = viewModel.description
        labelQuantity.text = "QTY: " + String(viewModel.quantity)
        
    }
}
