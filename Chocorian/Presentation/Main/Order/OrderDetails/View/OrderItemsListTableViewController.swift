//
//  OrderItemsListTableViewController.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import UIKit

final class OrderItemsListTableViewController: UITableViewController {

    var viewModel: OrderDetailsViewModel!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    func reload() {
        tableView.reloadData()
    }

    // MARK: - Private

    private func setupViews() {
        tableView.estimatedRowHeight = CartListItemCell.height
        tableView.rowHeight = UITableView.automaticDimension
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension OrderItemsListTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.value.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OrderDetailsItemCell.reuseIdentifier,
                                                       for: indexPath) as? OrderDetailsItemCell else {
            assertionFailure("Cannot dequeue reusable cell \(OrderDetailsItemCell.self) with reuseIdentifier: \(OrderDetailsItemCell.reuseIdentifier)")
            return UITableViewCell()
        }

        cell.fill(with: viewModel.items.value[indexPath.row])

        return cell
    }
}
