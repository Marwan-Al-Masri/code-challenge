//
//  OrderDetailsViewController.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import UIKit

final class OrderDetailsViewController: UIViewController, StoryboardInstantiable, Alertable {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var orderItemsListContainer: UIView!
    @IBOutlet var labelItemCount: UILabel!
    @IBOutlet var labelCreateDate: UILabel!
    @IBOutlet var labelTotal: UILabel!
    
    private var viewModel: OrderDetailsViewModel!

    private var orderItemsListTableViewController: OrderItemsListTableViewController?

    // MARK: - Lifecycle

    static func create(with viewModel: OrderDetailsViewModel) -> OrderDetailsViewController {
        let view = OrderDetailsViewController.instantiateViewController()
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bind(to: viewModel)
        viewModel.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Order Details"
    }

    private func bind(to viewModel: OrderDetailsViewModel) {
        viewModel.items.observe(on: self) { [weak self] _ in self?.updateItems() }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: OrderItemsListTableViewController.self),
            let destinationVC = segue.destination as? OrderItemsListTableViewController {
            orderItemsListTableViewController = destinationVC
            orderItemsListTableViewController?.viewModel = viewModel
        }
    }

    // MARK: - Private

    private func setupViews() {
    }

    private func updateItems() {
        orderItemsListTableViewController?.reload()
        labelItemCount.text = "\(viewModel.order.quantity)" + " items"
        labelCreateDate.text = viewModel.order.date
        labelTotal.text = String(format: "$%.2f", viewModel.order.totalPrice)
    }
}
