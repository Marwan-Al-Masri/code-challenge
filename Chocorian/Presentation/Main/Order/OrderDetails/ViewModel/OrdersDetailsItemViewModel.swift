//
//  OrdersDetailItemViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

struct OrdersDetailsItemViewModel: Equatable {
    let date: String
    let totalPrice: Double
    let quantity: Int
    let items: [ItemListViewModel]
    
}

extension OrdersDetailsItemViewModel {

    init(order: Order) {
        self.date = dateFormatter.string(from: order.createDate ?? Date())
        self.totalPrice = order.items.reduce(0.0) { $0 + $1.product.price * Double($1.quantity)}
        self.quantity = order.items.reduce(0) { $0 + $1.quantity }
        self.items = order.items.map { ItemListViewModel(product: $0.product, quantity: $0.quantity) }
    }
}
    
private let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    return formatter
}()
