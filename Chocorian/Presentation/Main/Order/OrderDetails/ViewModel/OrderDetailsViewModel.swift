//
//  OrderDetailViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

protocol OrderDetailsViewModelInput {
    func viewDidLoad()
}

protocol OrderDetailsViewModelOutput {
    var items: Observable<[ItemListViewModel]> { get }
    var order: OrdersDetailsItemViewModel { get }
}

protocol OrderDetailsViewModel: OrderDetailsViewModelInput, OrderDetailsViewModelOutput {}

final class DefaultOrderDetailsViewModel: OrderDetailsViewModel {

    public let order: OrdersDetailsItemViewModel

    // MARK: - OUTPUT

    let items: Observable<[ItemListViewModel]> = Observable([])

    // MARK: - Init

    init(order: Order) {
        self.order = OrdersDetailsItemViewModel(order: order)
    }
}

// MARK: - INPUT. View event methods

extension DefaultOrderDetailsViewModel {

    func viewDidLoad() {
        self.items.value = order.items
    }
}
