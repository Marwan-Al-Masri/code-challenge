//
//  OrdersListViewController.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import UIKit

final class OrdersListViewController: UIViewController, StoryboardInstantiable, Alertable {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var ordersListContainer: UIView!
    
    private var viewModel: OrdersListViewModel!

    private var ordersTableViewController: OrdersListTableViewController?

    // MARK: - Lifecycle

    static func create(with viewModel: OrdersListViewModel) -> OrdersListViewController {
        let view = OrdersListViewController.instantiateViewController()
        view.tabBarItem.image = UIImage(systemName: "shippingbox")
        view.tabBarItem.selectedImage = UIImage(systemName: "shippingbox.fill")
        view.tabBarItem.title = "Orders"
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bind(to: viewModel)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.title = "Orders"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.reload()
    }

    private func bind(to viewModel: OrdersListViewModel) {
        viewModel.items.observe(on: self) { [weak self] _ in self?.updateItems() }
        viewModel.loading.observe(on: self) { [weak self] in self?.updateLoading($0) }
        viewModel.error.observe(on: self) { [weak self] in self?.showError($0) }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing: OrdersListTableViewController.self),
            let destinationVC = segue.destination as? OrdersListTableViewController {
            ordersTableViewController = destinationVC
            ordersTableViewController?.viewModel = viewModel
        }
    }

    // MARK: - Private

    private func setupViews() {
    }

    private func updateItems() {
        ordersTableViewController?.reload()
    }

    private func updateLoading(_ loading: Bool) {
        loading ? LoadingView.show() : LoadingView.hide()
    }

    private func showError(_ error: String) {
        guard !error.isEmpty else { return }
        showAlert(title: "Error", message: error)
    }
}
