//
//  OrdersListItemCell.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import UIKit
import Kingfisher

final class OrdersListItemCell: UITableViewCell {

    static let reuseIdentifier = String(describing: OrdersListItemCell.self)
    static let height = CGFloat(130)
    
    @IBOutlet var labelCreateDate: UILabel!
    @IBOutlet var labelItemCount: UILabel!
    @IBOutlet var labelTotal: UILabel!

    private var viewModel: OrdersListItemViewModel!
    
    private var indexPath: IndexPath?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        labelCreateDate.text = nil
        labelItemCount.text = "0 items"
        labelTotal.text = "$0"
    }

    func fill(with viewModel: OrdersListItemViewModel,
              at indexPath: IndexPath) {
        self.viewModel = viewModel
        self.indexPath = indexPath
        
        labelCreateDate.text = viewModel.date
        labelItemCount.text = "\(viewModel.quantity)" + " items"
        labelTotal.text = String(format: "$%.2f", viewModel.totalPrice)
    }
}
