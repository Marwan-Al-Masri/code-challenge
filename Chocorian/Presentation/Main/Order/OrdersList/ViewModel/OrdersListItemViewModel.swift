//
//  OrdersListItemViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

struct OrdersListItemViewModel: Equatable {
    let date: String
    let totalPrice: Double
    let quantity: Int
}

extension OrdersListItemViewModel {

    init(order: Order) {
        self.date = dateFormatter.string(from: order.createDate ?? Date())
        self.totalPrice = order.items.reduce(0.0) { $0 + $1.product.price * Double($1.quantity)}
        self.quantity = order.items.reduce(0) { $0 + $1.quantity }
    }
}
    
private let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    return formatter
}()
