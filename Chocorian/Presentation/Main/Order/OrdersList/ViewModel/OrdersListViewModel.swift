//
//  OrdersListViewModel.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

struct OrdersListViewModelActions {
    let showOrderDetails: (Order) -> Void
}

protocol OrdersListViewModelInput {
    func didSelectItem(at index: Int)
    func reload()
}

protocol OrdersListViewModelOutput {
    var items: Observable<[OrdersListItemViewModel]> { get }
    var loading: Observable<Bool> { get }
    var error: Observable<String> { get }
}

protocol OrdersListViewModel: OrdersListViewModelInput, OrdersListViewModelOutput {}

final class DefaultOrdersListViewModel: OrdersListViewModel {

    private let fetchOrdersUseCase: FetchOrdersUseCase
    private let actions: OrdersListViewModelActions?

    // MARK: - OUTPUT

    let items: Observable<[OrdersListItemViewModel]> = Observable([])
    let loading: Observable<Bool> = Observable(false)
    let error: Observable<String> = Observable("")

    // MARK: - Init

    init(fetchOrdersUseCase: FetchOrdersUseCase,
         actions: OrdersListViewModelActions? = nil) {
        self.fetchOrdersUseCase = fetchOrdersUseCase
        self.actions = actions
    }

    // MARK: - Private
    
    var orders: [Order] = [] {
        didSet {
            self.items.value = orders.map { OrdersListItemViewModel(order: $0) }
        }
    }

    private func load() {
        self.loading.value = true
        fetchOrdersUseCase.execute(
            completion: { result in
                switch result {
                case .success(let orders):
                    self.orders = orders
                case .failure(let error):
                    self.handle(error: error)
                }
                self.loading.value = false
        })
    }

    private func handle(error: Error) {
        self.error.value = NSLocalizedString("Failed loading orders", comment: "")
    }
}

// MARK: - INPUT. View event methods

extension DefaultOrdersListViewModel {
    
    func reload() {
        load()
    }

    func didSelectItem(at index: Int) {
        actions?.showOrderDetails(orders[index])
    }
}
