//
//  DefaultProductRepository.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

final class DefaultProductRepository {

    private let dataTransferService: DataTransferService

    init(dataTransferService: DataTransferService) {
        self.dataTransferService = dataTransferService
    }
}

extension DefaultProductRepository: ProductRepository {

    public func fetchProcudts(token: String,
                              completion: @escaping (Result<[Product], Error>) -> Void) -> Cancellable? {

        let requestDTO = AuthreizedRequestDTO(token: token)
        let task = RepositoryTask()
        
        let endpoint = APIEndpoints.fetchProducts(with: requestDTO)
        task.networkTask = self.dataTransferService.request(with: endpoint) { result in
            switch result {
            case .success(let responseDTO):
                completion(.success(responseDTO.map { $0.toDomain() }))
            case .failure(let error):
                completion(.failure(error))
            }
        }
        return task
    }
}
