//
//  DefaultOrderRepository.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

final class DefaultOrderRepository {
    
    private let cache: OrderResponseStorage

    init(cache: OrderResponseStorage) {
        self.cache = cache
    }
}

extension DefaultOrderRepository: OrderRepository {
    
    func fetch(completion: @escaping (Result<[Order], Error>) -> Void) {
        cache.fetch(completion: completion)
    }
    
    func save(order: Order, completion: @escaping (Result<Void, Error>) -> Void) {
        cache.save(order: order, completion: completion)
    }
    
    func clear(completion: @escaping (Result<Void, Error>) -> Void) {
        cache.clear(completion: completion)
    }
}
