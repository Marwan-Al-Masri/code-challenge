//
//  DefaultAuthenticationRepository.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import Foundation

final class DefaultAuthenticationRepository {

    private let dataTransferService: DataTransferService

    init(dataTransferService: DataTransferService) {
        self.dataTransferService = dataTransferService
    }
}

extension DefaultAuthenticationRepository: AuthenticationRepository {

    public func login(email: String, password: String,
                      completion: @escaping (Result<Authentication, Error>) -> Void) -> Cancellable? {

        let requestDTO = AuthenticationRequestDTO(email: email, password: password)
        let task = RepositoryTask()

        let endpoint = APIEndpoints.login(with: requestDTO)
        task.networkTask = self.dataTransferService.request(with: endpoint) { result in
            switch result {
            case .success(let responseDTO):
                completion(.success(responseDTO.toDomain()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
        return task
    }
}
