//
//  DefaultCartRepository.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

final class DefaultCartRepository {
    
    private let cache: CartResponseStorage

    init(cache: CartResponseStorage) {
        self.cache = cache
    }
}

extension DefaultCartRepository: CartRepository {
    func fetch(completion: @escaping (Result<[Item], Error>) -> Void) {
        cache.get { result in
            switch result {
            case .success(let cartDto):
                let response = cartDto.map { $0.toDomain() }
                completion(.success(response))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func save(cart: [Item], completion: @escaping (Result<Void, Error>) -> Void) {
        let request = cart.map { CartRequestDTO.fromDomain($0) }
        cache.set(request: request, completion: completion)
    }
    
    func clear(completion: @escaping (Result<[Item], Error>) -> Void) {
        cache.clear { result in
            switch result {
            case .success(let cartDto):
                let response = cartDto.map { $0.toDomain() }
                completion(.success(response))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
