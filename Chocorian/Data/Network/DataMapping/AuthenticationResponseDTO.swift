//
//  AuthenticationResponseDTO.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import Foundation

// MARK: - Data Transfer Object

struct AuthenticationResponseDTO: Decodable {
    private enum CodingKeys: String, CodingKey {
        case token
    }
    let token: String
}

extension AuthenticationResponseDTO {
    func toDomain() -> Authentication {
        return .init(token: self.token)
    }
}
