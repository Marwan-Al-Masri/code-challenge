//
//  ProductsResponseDTO.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

struct ProductResponseDTO: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "name"
        case productDescription = "Description"
        case price = "price"
        case photo = "photo"
    }
    let id: String
    let name: String
    let productDescription: String
    let price: Double
    let photo: String
}

extension ProductResponseDTO {
    func toDomain() -> Product {
        return .init(id: self.id,
                     name: self.name,
                     productDescription: self.productDescription,
                     price: self.price,
                     photo: self.photo)
    }
}
