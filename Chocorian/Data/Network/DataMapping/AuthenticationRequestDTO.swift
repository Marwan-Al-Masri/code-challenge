//
//  AuthenticationRequestDTO.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import Foundation

struct AuthenticationRequestDTO: Encodable {
    let email: String
    let password: String
}
