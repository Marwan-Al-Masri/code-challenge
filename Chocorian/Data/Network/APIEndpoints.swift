//
//  APIEndpoints.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import Foundation

struct APIEndpoints {
    
    static func login(with loginRequestDTO: AuthenticationRequestDTO) -> Endpoint<AuthenticationResponseDTO> {

        return Endpoint(path: "choco/login",
                        method: .post,
                        bodyParamatersEncodable: loginRequestDTO)
    }
    
    static func fetchProducts(with authreizedsRequestDTO: AuthreizedRequestDTO) -> Endpoint<[ProductResponseDTO]> {

        return Endpoint(path: "choco/products",
                        method: .get,
                        queryParametersEncodable: authreizedsRequestDTO)
    }
}
