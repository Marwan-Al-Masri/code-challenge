//
//  CartUserDefaultStorage.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

enum CartUserDefaultStorageError: Error {
    case notFound
}

final class CartUserDefaultStorage {

    private let storage: UserDefaults
    
    private var storageKey: String

    init(storage: UserDefaults = UserDefaults.standard, storageKey: String = "USER-CART") {
        self.storage = storage
        self.storageKey = storageKey
    }
}

extension CartUserDefaultStorage: CartResponseStorage {
    
    func get(completion: @escaping (Result<[CartResponseDTO], Error>) -> Void) {
        
        guard let data = storage.object(forKey: self.storageKey) as? Data else {
            completion(.success([]))
            return
        }
        
        do {
            let response = try JSONDecoder().decode([CartResponseDTO].self, from: data)
            completion(.success(response))
        } catch {
            completion(.failure(error))
        }
    }
    
    func set(request: [CartRequestDTO], completion: @escaping (Result<Void, Error>) -> Void) {
        
        do {
            let data = try JSONEncoder().encode(request)
            storage.set(data, forKey: self.storageKey)
            storage.synchronize()
            completion(.success(()))
        } catch {
            completion(.failure(error))
        }
    }
    
    func clear(completion: @escaping (Result<[CartResponseDTO], Error>) -> Void) {
        get(completion: completion)
        storage.removeObject(forKey: self.storageKey)
        storage.synchronize()
    }
}
