//
//  CartResponseStorage.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

protocol CartResponseStorage {
    func get(completion: @escaping (Result<[CartResponseDTO], Error>) -> Void)
    func set(request: [CartRequestDTO], completion: @escaping (Result<Void, Error>) -> Void)
    func clear(completion: @escaping (Result<[CartResponseDTO], Error>) -> Void)
}
