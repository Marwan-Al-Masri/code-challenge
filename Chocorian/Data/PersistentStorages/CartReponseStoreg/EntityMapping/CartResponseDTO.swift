//
//  CartResponseDTO.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

struct CartResponseDTO: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case productDescription
        case price
        case photo
        case quntity
    }
    let id: String
    let name: String
    let productDescription: String
    let price: Double
    let photo: String
    let quntity: Int
}

extension CartResponseDTO {
    func toDomain() -> Item {
        let product = Product(id: id, name: name, productDescription: productDescription, price: price, photo: photo)
        return .init(product: product, quantity: quntity)
    }
}
