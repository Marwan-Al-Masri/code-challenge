//
//  CartRequestDTO.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

struct CartRequestDTO: Encodable {
    let id: String
    let name: String
    let productDescription: String
    let price: Double
    let photo: String?
    let quntity: Int
    
    static func fromDomain(_ item: Item) -> CartRequestDTO {
        return CartRequestDTO(id: item.product.id,
                              name: item.product.name,
                              productDescription: item.product.productDescription,
                              price: item.product.price,
                              photo: item.product.photo,
                              quntity: item.quantity)
    }
}
