//
//  CoreDataOrderResponseStorage.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation
import CoreData

final class CoreDataOrderResponseStorage {

    private let coreDataStorage: CoreDataStorage

    init(coreDataStorage: CoreDataStorage = CoreDataStorage.shared) {
        self.coreDataStorage = coreDataStorage
    }
    
    private func deleteAllObject(for entityName: String, in context: NSManagedObjectContext) -> Error? {
        
            let fetchItemsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            do {
                let objects = try context.fetch(fetchItemsRequest)
                for object in objects as! [NSManagedObject] {
                    context.delete(object)
                }
                try context.save()
                return nil
            } catch {
                return error
            }
    }
}

extension CoreDataOrderResponseStorage: OrderResponseStorage {
    func save(order: Order, completion: @escaping (Result<Void, Error>) -> Void) {
        
        coreDataStorage.performBackgroundTask { context in
            _ = order.toEntity(in: context)
            do {
                try context.save()
                completion(.success(()))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    func fetch(completion: @escaping (Result<[Order], Error>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderEntity")
            do {
                let objects = try context.fetch(fetchRequest)
                var result: [Order] = []
                for object in objects as! [NSManagedObject] {
                    if let order = object as? OrderEntity {
                        result.append(order.toDomain())
                    }
                }
                completion(.success(result))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    func clear(completion: @escaping (Result<Void, Error>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            if let error = self.deleteAllObject(for: "OrderEntity", in: context) {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }
}
