//
//  ItemEntity+Mapping.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation
import CoreData

extension ItemEntity {
    func toDomain() -> Item {
        let product = Product(id: productId ?? "",
                              name: productName ?? "",
                              productDescription: productDescription ?? "",
                              price: price,
                              photo: photoPath)
        return .init(product: product, quantity: Int(quantity))
    }
}

extension Item {
    func toEntity(in context: NSManagedObjectContext) -> ItemEntity {
        let entity: ItemEntity = .init(context: context)
        entity.quantity = Int16(quantity)
        entity.productId = product.id
        entity.productName = product.name
        entity.productDescription = product.productDescription
        entity.price = product.price
        entity.photoPath = product.photo
        return entity
    }
}
