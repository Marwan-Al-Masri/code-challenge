//
//  OrderEntity+Mapping.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation
import CoreData

extension OrderEntity {
    
    public var itemsArray: [ItemEntity] {
        let set = items as? Set<ItemEntity> ?? []
        return set.sorted {
            $0.id < $1.id
        }
    }
    
    func toDomain() -> Order {
        return .init(id: id ?? "", createDate: createdAt, items: itemsArray.map { $0.toDomain() })
    }
}

extension Order {
    func toEntity(in context: NSManagedObjectContext) -> OrderEntity {
        let entity: OrderEntity = .init(context: context)
        entity.id = id
        entity.createdAt = createDate
        let entityItems = items.map { $0.toEntity(in: context) }
        entity.items = NSSet(array: entityItems)
        return entity
    }
}
