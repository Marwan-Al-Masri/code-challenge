//
//  Product.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

struct Product {
    let id: String
    let name: String
    let productDescription: String
    let price: Double
    let photo: String?
}
