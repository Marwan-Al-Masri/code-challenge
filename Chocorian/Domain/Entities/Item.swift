//
//  Item.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

struct Item {
    let product: Product
    let quantity: Int
}
