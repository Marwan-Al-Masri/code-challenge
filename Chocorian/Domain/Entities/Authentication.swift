//
//  Authorization.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import Foundation

struct Authentication: Equatable {
    let token: String
}
