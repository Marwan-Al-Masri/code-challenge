//
//  Order.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

struct Order {
    let id: String
    let createDate: Date?
    let items: [Item]
}
