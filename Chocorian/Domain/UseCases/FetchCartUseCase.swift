//
//  FetchCartUseCase.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

protocol FetchCartUseCase {
    
    func execute(completion: @escaping (Result<[Item], Error>) -> Void)
}

final class DefaultFetchCartUseCase: FetchCartUseCase {
    private let cartRepository: CartRepository

    init(cartRepository: CartRepository) {
        self.cartRepository = cartRepository
    }
    
    func execute(completion: @escaping (Result<[Item], Error>) -> Void) {
    
        cartRepository.fetch(completion: { result in
            completion(result)
        })
    }
}
