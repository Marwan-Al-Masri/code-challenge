//
//  ClearOrdersUseCase.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

protocol ClearOrdersUseCase {
    
    func execute(completion: @escaping (Result<Void, Error>) -> Void)
}

final class DefaultClearOrdersUseCase: ClearOrdersUseCase {
    private let orderRepository: OrderRepository

    init(orderRepository: OrderRepository) {
        self.orderRepository = orderRepository
    }
    
    func execute(completion: @escaping (Result<Void, Error>) -> Void) {
    
        orderRepository.clear(completion: { result in
            completion(result)
        })
    }
}
