//
//  FetchOrdersUseCase.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

protocol FetchOrdersUseCase {
    
    func execute(completion: @escaping (Result<[Order], Error>) -> Void)
}

final class DefaultFetchOrdersUseCase: FetchOrdersUseCase {
    private let orderRepository: OrderRepository

    init(orderRepository: OrderRepository) {
        self.orderRepository = orderRepository
    }
    
    func execute(completion: @escaping (Result<[Order], Error>) -> Void) {
    
        orderRepository.fetch(completion: { result in
            completion(result)
        })
    }
}
