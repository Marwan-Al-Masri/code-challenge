//
//  AuthenticationUseCase.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import Foundation

protocol AuthenticationUseCase {
    
    @discardableResult
    func execute(requestValue: AuthenticationUseCaseRequestValue,
                 completion: @escaping (Result<Authentication, Error>) -> Void) -> Cancellable?
}

final class DefaultAuthenticationUseCase: AuthenticationUseCase {

    private let authenticationRepository: AuthenticationRepository

    init(authenticationRepository: AuthenticationRepository) {

        self.authenticationRepository = authenticationRepository
    }

    func execute(requestValue: AuthenticationUseCaseRequestValue,
                 completion: @escaping (Result<Authentication, Error>) -> Void) -> Cancellable? {

        return authenticationRepository.login(email: requestValue.email,
                                              password: requestValue.password,
                                              completion: { result in
            completion(result)
        })
    }
}

struct AuthenticationUseCaseRequestValue {
    let email: String
    let password: String
}
