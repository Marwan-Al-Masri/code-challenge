//
//  SaveOrderUseCase.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

protocol SaveOrderUseCase {
    
    func execute(order: Order, completion: @escaping (Result<Void, Error>) -> Void)
}

final class DefaultSaveOrderUseCase: SaveOrderUseCase {
    private let orderRepository: OrderRepository

    init(orderRepository: OrderRepository) {
        self.orderRepository = orderRepository
    }
    
    func execute(order: Order, completion: @escaping (Result<Void, Error>) -> Void) {
    
        orderRepository.save(order: order, completion: { result in
            completion(result)
        })
    }
}
