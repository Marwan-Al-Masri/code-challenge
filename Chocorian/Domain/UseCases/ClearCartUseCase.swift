//
//  ClearCartUseCase.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

protocol ClearCartUseCase {
    
    func execute(completion: @escaping (Result<[Item], Error>) -> Void)
}

final class DefaultClearCartUseCase: ClearCartUseCase {
    private let cartRepository: CartRepository

    init(cartRepository: CartRepository) {
        self.cartRepository = cartRepository
    }
    
    func execute(completion: @escaping (Result<[Item], Error>) -> Void) {
    
        cartRepository.clear(completion: { result in
            completion(result)
        })
    }
}
