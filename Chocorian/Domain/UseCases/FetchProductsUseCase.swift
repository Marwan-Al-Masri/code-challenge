//
//  FetchProcudtsUseCase.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

protocol FetchProductsUseCase {
    
    @discardableResult
    func execute(token: String,
                 completion: @escaping (Result<[Product], Error>) -> Void) -> Cancellable?
}

final class DefaultFetchProductsUseCase: FetchProductsUseCase {
    let token: String
    private let procudtsRepository: ProductRepository

    init(token: String,
         procudtsRepository: ProductRepository) {

        self.token = token
        self.procudtsRepository = procudtsRepository
    }
    
    func execute(token: String,
                 completion: @escaping (Result<[Product], Error>) -> Void) -> Cancellable? {
    
        return procudtsRepository.fetchProcudts(token: token,
                                                 completion: { result in
            completion(result)
        })
    }
}
