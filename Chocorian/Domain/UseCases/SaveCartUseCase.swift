//
//  SaveCartUseCase.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

protocol SaveCartUseCase {
    
    func execute(cart: [Item], completion: @escaping (Result<Void, Error>) -> Void)
}

final class DefaultSaveCartUseCase: SaveCartUseCase {
    private let cartRepository: CartRepository

    init(cartRepository: CartRepository) {
        self.cartRepository = cartRepository
    }
    
    func execute(cart: [Item], completion: @escaping (Result<Void, Error>) -> Void) {
    
        cartRepository.save(cart: cart, completion: { result in
            completion(result)
        })
    }
}
