//
//  LoginRepository.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/25/21.
//

import Foundation

protocol AuthenticationRepository {
    @discardableResult
    func login(email: String, password: String,
               completion: @escaping (Result<Authentication, Error>) -> Void) -> Cancellable?
}
