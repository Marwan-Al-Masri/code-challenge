//
//  OrderRepository.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation

protocol OrderRepository {
    func fetch(completion: @escaping (Result<[Order], Error>) -> Void)
    func save(order: Order, completion: @escaping (Result<Void, Error>) -> Void)
    func clear(completion: @escaping (Result<Void, Error>) -> Void)
}
