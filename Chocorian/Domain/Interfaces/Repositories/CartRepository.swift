//
//  CartRepository.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

protocol CartRepository {
    func fetch(completion: @escaping (Result<[Item], Error>) -> Void)
    func save(cart: [Item], completion: @escaping (Result<Void, Error>) -> Void)
    func clear(completion: @escaping (Result<[Item], Error>) -> Void)
}
