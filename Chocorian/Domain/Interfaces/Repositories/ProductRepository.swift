//
//  ProcudtsRepository.swift
//  Chocorian
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation

protocol ProductRepository {
    func fetchProcudts(token: String,
                       completion: @escaping (Result<[Product], Error>) -> Void) -> Cancellable?
}
