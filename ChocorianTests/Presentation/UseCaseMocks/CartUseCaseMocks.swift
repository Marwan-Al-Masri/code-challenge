//
//  CartUseCaseMocks.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

enum CartUseCaseError: Error {
    case someError
}

class FetchCartUseCaseMock: FetchCartUseCase {
    var expectation: XCTestExpectation?
    var error: Error?
    
    let cart: [Item] = {
        Item.stub()
    }()
    
    func execute(completion: @escaping (Result<[Item], Error>) -> Void) {
        if let error = error {
            completion(.failure(error))
        } else {
            completion(.success(cart))
        }
        expectation?.fulfill()
    }
}

class SaveCartUseCaseMock: SaveCartUseCase {
    var expectation: XCTestExpectation?
    var error: Error?
    
    func execute(cart: [Item], completion: @escaping (Result<Void, Error>) -> Void) {
        if let error = error {
            completion(.failure(error))
        } else {
            completion(.success(()))
        }
        expectation?.fulfill()
    }
}

class ClearCartUseCaseMock: ClearCartUseCase {
    var expectation: XCTestExpectation?
    var error: Error?
    
    let cart: [Item] = {
        Item.stub()
    }()
    
    func execute(completion: @escaping (Result<[Item], Error>) -> Void) {
        if let error = error {
            completion(.failure(error))
        } else {
            completion(.success(cart))
        }
        expectation?.fulfill()
    }
}
