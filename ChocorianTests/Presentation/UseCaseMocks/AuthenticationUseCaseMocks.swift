//
//  AuthenticationUseCaseMocks.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

enum AuthenticationUseCaseError: Error {
    case someError
}

class AuthenticationUseCaseMock: AuthenticationUseCase {
    var expectation: XCTestExpectation?
    var error: Error?
    var authentication = Authentication(token: "1234567890")
    
    func execute(requestValue: AuthenticationUseCaseRequestValue,
                 completion: @escaping (Result<Authentication, Error>) -> Void) -> Cancellable? {
        if let error = error {
            completion(.failure(error))
        } else {
            completion(.success(authentication))
        }
        expectation?.fulfill()
        return nil
    }
}
