//
//  OrderUseCaseMocks.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

enum OrderUseCaseError: Error {
    case someError
}

class FetchOrdersUseCaseMock: FetchOrdersUseCase {
    var expectation: XCTestExpectation?
    var error: Error?
    
    let orders: [Order] = {
        Order.stub()
    }()
    
    func execute(completion: @escaping (Result<[Order], Error>) -> Void) {
        if let error = error {
            completion(.failure(error))
        } else {
            completion(.success(orders))
        }
        expectation?.fulfill()
    }
}

class SaveOrderUseCaseMock: SaveOrderUseCase {
    var expectation: XCTestExpectation?
    var error: Error?
    
    func execute(order: Order, completion: @escaping (Result<Void, Error>) -> Void) {
        if let error = error {
            completion(.failure(error))
        } else {
            completion(.success(()))
        }
        expectation?.fulfill()
    }
}

class ClearOrdersUseCaseMock: ClearOrdersUseCase {
    var expectation: XCTestExpectation?
    var error: Error?
    
    func execute(completion: @escaping (Result<Void, Error>) -> Void) {
        if let error = error {
            completion(.failure(error))
        } else {
            completion(.success(()))
        }
        expectation?.fulfill()
    }
}
