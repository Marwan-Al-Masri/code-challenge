//
//  ProductsUseCaseMocks.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

enum ProductsUseCaseError: Error {
    case someError
}

class FetchProductsUseCaseMock: FetchProductsUseCase {
    var expectation: XCTestExpectation?
    var error: Error?
    
    let products: [Product] = {
        Product.stub()
    }()
    
    func execute(token: String,
                 completion: @escaping (Result<[Product], Error>) -> Void) -> Cancellable? {
        if let error = error {
            completion(.failure(error))
        } else {
            completion(.success(products))
        }
        expectation?.fulfill()
        return nil
    }
}
