//
//  LoginViewModelTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/26/21.
//

import XCTest
@testable import Chocorian

class LoginViewModelTests: XCTestCase {
    
    static let authentication: Authentication = Authentication(token: "1234567890")
    var task: Cancellable?
    
    func test_whenAuthenticationUseCaseSuccessfullyLogin_thenViewModelContainsAuthentecationObject() {
        // given
        let authenticationUseCaseMock = AuthenticationUseCaseMock()
        authenticationUseCaseMock.expectation = self.expectation(description: "contains authentication object")
        authenticationUseCaseMock.authentication = Self.authentication
        let appConfiguration = AppConfigurationMocks()
        let viewModel = DefaultLoginViewModel(authenticationUseCase: authenticationUseCaseMock, appConfiguration: appConfiguration)
        // when
        viewModel.login(email: "", password: "")
        
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(viewModel.authentication?.token)
        XCTAssertEqual(viewModel.authentication?.token, Self.authentication.token)
    }
    
    func test_whenAuthenticationUseCaseReturnsError_thenViewModelContainsError() {
        // given
        let authenticationUseCaseMock = AuthenticationUseCaseMock()
        authenticationUseCaseMock.expectation = self.expectation(description: "contain errors that say \"Wrong Email or Passowrd\"")
        authenticationUseCaseMock.error = AuthenticationUseCaseError.someError
        let appConfiguration = AppConfigurationMocks()
        let viewModel = DefaultLoginViewModel(authenticationUseCase: authenticationUseCaseMock, appConfiguration: appConfiguration)
        // when
        viewModel.login(email: "", password: "")
        
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(viewModel.authentication?.token)
        XCTAssertNotNil(viewModel.error)
        XCTAssertEqual(viewModel.error.value, "Wrong Email or Passowrd")
    }
}
