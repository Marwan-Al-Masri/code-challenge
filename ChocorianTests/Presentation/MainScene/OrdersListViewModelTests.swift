//
//  OrdersListViewModelTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

class OrdersListViewModelTests: XCTestCase {
    
    func test_whenFetchOrdersUseCaseRetrievesSuccessfully_thenViewModelContainsItems() {
        // given
        let fetchOrdersUseCaseMock = FetchOrdersUseCaseMock()
        fetchOrdersUseCaseMock.expectation = self.expectation(description: "contain 3 items")
        let viewModel = DefaultOrdersListViewModel(fetchOrdersUseCase: fetchOrdersUseCaseMock)
        // when
        viewModel.reload()
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(viewModel.items.value.count, 4)
    }
    
    func test_whenFetchOrdersUseCaseReturnsError_thenViewModelContainsError() {
        // given
        let fetchOrdersUseCaseMock = FetchOrdersUseCaseMock()
        fetchOrdersUseCaseMock.expectation = self.expectation(description: "contain error")
        fetchOrdersUseCaseMock.error = OrderUseCaseError.someError
        let viewModel = DefaultOrdersListViewModel(fetchOrdersUseCase: fetchOrdersUseCaseMock)
        // when
        viewModel.reload()
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(viewModel.error)
        XCTAssertEqual(viewModel.items.value.count, 0)
    }
}
