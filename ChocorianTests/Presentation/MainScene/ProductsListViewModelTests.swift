//
//  ProductsListViewModelTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/26/21.
//

import XCTest
@testable import Chocorian

class ProductsListViewModelTests: XCTestCase {
    
    func test_whenFetchProductsUseCaseRetrievesSuccessfully_thenViewModelContainsItems() {
        // given
        let fetchProductsUseCaseMock = FetchProductsUseCaseMock()
        fetchProductsUseCaseMock.expectation = self.expectation(description: "contain 3 items")
        let fetchCartUseCaseMock = FetchCartUseCaseMock()
        let saveCartUseCaseMock = SaveCartUseCaseMock()
        let appConfiguration = AppConfigurationMocks()
        appConfiguration.apiToken = "1234567890"
        let viewModel = DefaultProductsListViewModel(fetchProductsUseCase: fetchProductsUseCaseMock,
                                                     fetchCartUseCase: fetchCartUseCaseMock,
                                                     saveCartUseCase: saveCartUseCaseMock,
                                                     appConfiguration: appConfiguration)
        // when
        viewModel.load()
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(viewModel.items.value.count, 3)
    }
    
    func test_whenFetchProductsUseCaseReturnsError_thenViewModelContainsError() {
        // given
        let fetchProductsUseCaseMock = FetchProductsUseCaseMock()
        fetchProductsUseCaseMock.expectation = self.expectation(description: "contain error")
        fetchProductsUseCaseMock.error = ProductsUseCaseError.someError
        let fetchCartUseCaseMock = FetchCartUseCaseMock()
        let saveCartUseCaseMock = SaveCartUseCaseMock()
        let appConfiguration = AppConfigurationMocks()
        appConfiguration.apiToken = "1234567890"
        let viewModel = DefaultProductsListViewModel(fetchProductsUseCase: fetchProductsUseCaseMock,
                                                     fetchCartUseCase: fetchCartUseCaseMock,
                                                     saveCartUseCase: saveCartUseCaseMock,
                                                     appConfiguration: appConfiguration)
        // when
        viewModel.load()
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(viewModel.error)
        XCTAssertEqual(viewModel.items.value.count, 0)
    }
    
    func test_whenFetchProductsUseCaseReturnsAuthError_thenViewModelContainsError() {
        // given
        let fetchProductsUseCaseMock = FetchProductsUseCaseMock()
        let expectation = self.expectation(description: "contain auth error")
        fetchProductsUseCaseMock.expectation = expectation
        fetchProductsUseCaseMock.error = ProductsUseCaseError.someError
        let fetchCartUseCaseMock = FetchCartUseCaseMock()
        let saveCartUseCaseMock = SaveCartUseCaseMock()
        let appConfiguration = AppConfigurationMocks()
        let viewModel = DefaultProductsListViewModel(fetchProductsUseCase: fetchProductsUseCaseMock,
                                                     fetchCartUseCase: fetchCartUseCaseMock,
                                                     saveCartUseCase: saveCartUseCaseMock,
                                                     appConfiguration: appConfiguration)
        // when
        viewModel.load()
        // then
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(viewModel.error)
        XCTAssertEqual(viewModel.items.value.count, 0)
    }
}
