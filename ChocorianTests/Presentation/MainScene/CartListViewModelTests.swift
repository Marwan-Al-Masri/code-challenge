//
//  CartListViewModelTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

class CartListViewModelTests: XCTestCase {
    
    func test_whenFetchCartUseCaseRetrievesSuccessfully_thenViewModelContainsItems() {
        // given
        let expectation = self.expectation(description: "contain 3 items")
        expectation.expectedFulfillmentCount = 1
        let fetchCartUseCaseMock = FetchCartUseCaseMock()
        fetchCartUseCaseMock.expectation = expectation
        let saveCartUseCaseMock = SaveCartUseCaseMock()
        let clearCartUseCaseMock = ClearCartUseCaseMock()
        let saveOrderUseCaseMock = SaveOrderUseCaseMock()
        let appConfiguration = AppConfigurationMocks()
        let viewModel = DefaultCartListViewModel(fetchCartUseCase: fetchCartUseCaseMock,
                                                 saveCartUseCase: saveCartUseCaseMock,
                                                 clearCartUseCase: clearCartUseCaseMock,
                                                 saveOrderUseCase: saveOrderUseCaseMock,
                                                 appConfiguration: appConfiguration)
        // when
        viewModel.load()
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(viewModel.items.value.count, 3)
    }
    
    func test_whenFetchCartUseCaseReturnsError_thenViewModelContainsError() {
        // given
        let expectation = self.expectation(description: "contain error")
        expectation.expectedFulfillmentCount = 1
        let fetchCartUseCaseMock = FetchCartUseCaseMock()
        fetchCartUseCaseMock.expectation = expectation
        fetchCartUseCaseMock.error = CartUseCaseError.someError
        let saveCartUseCaseMock = SaveCartUseCaseMock()
        let clearCartUseCaseMock = ClearCartUseCaseMock()
        let saveOrderUseCaseMock = SaveOrderUseCaseMock()
        let appConfiguration = AppConfigurationMocks()
        let viewModel = DefaultCartListViewModel(fetchCartUseCase: fetchCartUseCaseMock,
                                                 saveCartUseCase: saveCartUseCaseMock,
                                                 clearCartUseCase: clearCartUseCaseMock,
                                                 saveOrderUseCase: saveOrderUseCaseMock,
                                                 appConfiguration: appConfiguration)
        // when
        viewModel.load()
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(viewModel.error)
        XCTAssertEqual(viewModel.items.value.count, 0)
    }
    
    func test_whenSaveCartUseCaseRetrievesSuccessfully_thenViewModelContainsItems() {
        // given
        let expectation = self.expectation(description: "first item to be updatedd items")
        expectation.expectedFulfillmentCount = 1
        let fetchCartUseCaseMock = FetchCartUseCaseMock()
        let saveCartUseCaseMock = SaveCartUseCaseMock()
        saveCartUseCaseMock.expectation = expectation
        let clearCartUseCaseMock = ClearCartUseCaseMock()
        let saveOrderUseCaseMock = SaveOrderUseCaseMock()
        let appConfiguration = AppConfigurationMocks()
        let viewModel = DefaultCartListViewModel(fetchCartUseCase: fetchCartUseCaseMock,
                                                 saveCartUseCase: saveCartUseCaseMock,
                                                 clearCartUseCase: clearCartUseCaseMock,
                                                 saveOrderUseCase: saveOrderUseCaseMock,
                                                 appConfiguration: appConfiguration)
        // when
        viewModel.load()
        viewModel.items.value[0].quantity = 2
        viewModel.update()
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(viewModel.items.value[0].quantity, 2)
    }
    
    func test_whenSaveCartUseCaseReturnsError_thenViewModelContainsError() {
        // given
        let expectation = self.expectation(description: "contain error")
        expectation.expectedFulfillmentCount = 1
        let fetchCartUseCaseMock = FetchCartUseCaseMock()
        let saveCartUseCaseMock = SaveCartUseCaseMock()
        saveCartUseCaseMock.expectation = expectation
        saveCartUseCaseMock.error = CartUseCaseError.someError
        let clearCartUseCaseMock = ClearCartUseCaseMock()
        let saveOrderUseCaseMock = SaveOrderUseCaseMock()
        let appConfiguration = AppConfigurationMocks()
        let viewModel = DefaultCartListViewModel(fetchCartUseCase: fetchCartUseCaseMock,
                                                 saveCartUseCase: saveCartUseCaseMock,
                                                 clearCartUseCase: clearCartUseCaseMock,
                                                 saveOrderUseCase: saveOrderUseCaseMock,
                                                 appConfiguration: appConfiguration)
        // when
        viewModel.load()
        viewModel.items.value[0].quantity = 2
        viewModel.update()
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(viewModel.error)
    }
    
    func test_whenclearCartUseCaseRetrievesSuccessfully_thenViewModelContainsZeroItems() {
        // given
        let expectation = self.expectation(description: "first item to be updatedd items")
        expectation.expectedFulfillmentCount = 1
        let fetchCartUseCaseMock = FetchCartUseCaseMock()
        let saveCartUseCaseMock = SaveCartUseCaseMock()
        let clearCartUseCaseMock = ClearCartUseCaseMock()
        clearCartUseCaseMock.expectation = expectation
        let saveOrderUseCaseMock = SaveOrderUseCaseMock()
        let appConfiguration = AppConfigurationMocks()
        let viewModel = DefaultCartListViewModel(fetchCartUseCase: fetchCartUseCaseMock,
                                                 saveCartUseCase: saveCartUseCaseMock,
                                                 clearCartUseCase: clearCartUseCaseMock,
                                                 saveOrderUseCase: saveOrderUseCaseMock,
                                                 appConfiguration: appConfiguration)
        // when
        viewModel.load()
        viewModel.clear()
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(viewModel.items.value.count, 0)
    }
    
    func test_whenClearCartUseCaseReturnsError_thenViewModelContainsErrorAndItems() {
        // given
        let expectation = self.expectation(description: "contain error and items")
        expectation.expectedFulfillmentCount = 1
        let fetchCartUseCaseMock = FetchCartUseCaseMock()
        let saveCartUseCaseMock = SaveCartUseCaseMock()
        let clearCartUseCaseMock = ClearCartUseCaseMock()
        clearCartUseCaseMock.expectation = expectation
        clearCartUseCaseMock.error = CartUseCaseError.someError
        let saveOrderUseCaseMock = SaveOrderUseCaseMock()
        let appConfiguration = AppConfigurationMocks()
        let viewModel = DefaultCartListViewModel(fetchCartUseCase: fetchCartUseCaseMock,
                                                 saveCartUseCase: saveCartUseCaseMock,
                                                 clearCartUseCase: clearCartUseCaseMock,
                                                 saveOrderUseCase: saveOrderUseCaseMock,
                                                 appConfiguration: appConfiguration)
        // when
        viewModel.load()
        viewModel.clear()
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(viewModel.error)
        XCTAssertEqual(viewModel.items.value.count, 3)
    }
}
