//
//  SettingsViewModelTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/26/21.
//

import XCTest
@testable import Chocorian

class SettingsViewModelTests: XCTestCase {
    
    func test_whenLogout_thenViewModelSetApiTokenToNil() {
        // given
        let expectation = self.expectation(description: "app configuration api token is nil")
        let clearCartUseCaseMock = ClearCartUseCaseMock()
        let clearOrdersUseCaseMock = ClearOrdersUseCaseMock()
        let appConfiguration = AppConfigurationMocks()
        appConfiguration.apiToken = "1234567890"
        let viewModel = DefaultSettingsViewModel(clearCartUseCase: clearCartUseCaseMock,
                                                 clearOrdersUseCase: clearOrdersUseCaseMock,
                                                 appConfiguration: appConfiguration)
        // when
        viewModel.logout()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            expectation.fulfill()
        }
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(appConfiguration.apiToken)
    }
}
