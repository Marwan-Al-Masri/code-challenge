//
//  Item+Stub.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation
@testable import Chocorian

extension Item {
    static func stub() -> [Self] {
        Product.stub().map { Self.init(product: $0, quantity: 1) }
    }
}
