//
//  Product+Stub.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation
@testable import Chocorian

extension Product {
    static func stub() -> [Self] {
        return [
            Product(
                id: "5e8c3c48-af49-425b-a6d9-f37f3511e4fa",
                name: "Product 1",
                productDescription :"This is product 1",
                price :100,
                photo: "http://xyz.com/prod1.jpg"),
            Product(
                id: "5e8c3c48-af49-425b-a6d9-f37f3521e4fa",
                name: "Product 2",
                productDescription: "This is product 2",
                price: 200,
                photo: "http://xyz.com/prod2.jpg"),
            Product(
                id:"5e8c3c48-af49-425b-a6d9-f37f3531e4fa",
                name: "Product 3",
                productDescription: "This is product 3",
                price: 300,
                photo: "http://xyz.com/prod3.jpg")
        ]
    }
}
