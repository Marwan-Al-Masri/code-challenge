//
//  Order+Stub.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import Foundation
@testable import Chocorian

extension Order {
    static func stub() -> [Self] {
        var orders: [Self] = []
        for i in 1...4 {
            orders.append(Self.init(id: "\(i)", createDate: Date(), items: Item.stub()))
        }
        return orders
    }
}
