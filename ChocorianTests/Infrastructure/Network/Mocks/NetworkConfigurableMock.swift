//
//  NetworkConfigurableMock.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation
@testable import Chocorian

class NetworkConfigurableMock: NetworkConfigurable {
    var baseURL: URL = URL(string: "https://mock.test.com")!
    var headers: [String: String] = [:]
    var queryParameters: [String: String] = [:]
}
