//
//  NetworkSessionManagerMock.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation
@testable import Chocorian

struct NetworkSessionManagerMock: NetworkSessionManager {
    let response: HTTPURLResponse?
    let data: Data?
    let error: Error?
    
    func request(_ request: URLRequest,
                 completion: @escaping CompletionHandler) -> NetworkCancellable {
        completion(data, response, error)
        return URLSessionDataTask()
    }
}
