//
//  AppConfigurationMocks.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/26/21.
//

import Foundation
@testable import Chocorian

class AppConfigurationMocks: AppConfiguration {
    var apiBaseURL: String = "https://mock.test.com"
    var apiToken: String? = nil
}
