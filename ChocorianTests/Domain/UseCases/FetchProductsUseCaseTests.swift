//
//  FetchProductsUseCaseTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/26/21.
//

import XCTest
@testable import Chocorian

class FetchProductsUseCaseTests: XCTestCase {
    
    static let products: [Product] = {
        Product.stub()
    }()
    
    func testFetchProductsUseCase_whenSuccessfullyFetchesProducts() {
        // given
        let expectation = self.expectation(description: "Fetch list of products successfully")
        let productRepository = ProductRepositoryMock(result: .success(Self.products))
        let useCase = DefaultFetchProductsUseCase(token: "1234567890", procudtsRepository: productRepository)

        // when
        var responseValue: [Product]?
        _ = useCase.execute(token: "1234567890", completion: { result in
            switch result {
            case .success(let value):
                responseValue = value
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(responseValue)
        XCTAssertEqual(responseValue?.count, Self.products.count)
    }
    
    func testFetchProductsUseCase_whenFailedFetchingProducts() {
        // given
        let expectation = self.expectation(description: "Fetch list of products to fail")
        expectation.expectedFulfillmentCount = 1
        let productRepository = ProductRepositoryMock(result: .failure(ProductsRepositorySuccessTestError.failedFetching))
        let useCase = DefaultFetchProductsUseCase(token: "1234567890", procudtsRepository: productRepository)

        // when
        var responseValue: [Product]?
        _ = useCase.execute(token: "1234567890", completion: { result in
            switch result {
            case .success(let value):
                responseValue = value
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(responseValue)
    }
}
