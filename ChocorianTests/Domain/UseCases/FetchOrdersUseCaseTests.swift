//
//  FetchOrdersUseCaseTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

class FetchOrdersUseCaseTests: XCTestCase {
    
    static let order: [Order] = {
        Order.stub()
    }()
    
    func testFetchOrderUseCase_whenSuccessfullyFetchesOrder() {
        // given
        let expectation = self.expectation(description: "Fetch order successfully")
        let orderRepository = OrderRepositoryMock(fetchResult: .success(Self.order),
                                                  saveResult: .success(()),
                                                  clearResult: .success(()))
        let useCase = DefaultFetchOrdersUseCase(orderRepository: orderRepository)

        // when
        var responseValue: [Order]?
        useCase.execute(completion: { result in
            switch result {
            case .success(let value):
                responseValue = value
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(responseValue)
        XCTAssertEqual(responseValue?.count, Self.order.count)
    }
    
    func testFetchOrderUseCase_whenFailedFetchingOrder() {
        // given
        let expectation = self.expectation(description: "Fetch order to fail")
        expectation.expectedFulfillmentCount = 1
        let orderRepository = OrderRepositoryMock(fetchResult: .failure(OrderRepositorySuccessTestError.failedFetching),
                                                  saveResult: .success(()),
                                                  clearResult: .success(()))
        let useCase = DefaultFetchOrdersUseCase(orderRepository: orderRepository)

        // when
        var responseValue: [Order]?
        useCase.execute(completion: { result in
            switch result {
            case .success(let value):
                responseValue = value
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(responseValue)
    }
}
