//
//  FetchCartUseCaseTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

class FetchCartUseCaseTests: XCTestCase {
    
    static let cart: [Item] = {
        Item.stub()
    }()
    
    func testFetchCartUseCase_whenSuccessfullyFetchesCart() {
        // given
        let expectation = self.expectation(description: "Fetch cart successfully")
        let cartRepository = CartRepositoryMock(fetchResult: .success(Self.cart),
                                                saveResult: .success(()),
                                                clearResult: .success([]))
        let useCase = DefaultFetchCartUseCase(cartRepository: cartRepository)

        // when
        var responseValue: [Item]?
        useCase.execute(completion: { result in
            switch result {
            case .success(let value):
                responseValue = value
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(responseValue)
        XCTAssertEqual(responseValue?.count, Self.cart.count)
    }
    
    func testFetchCartUseCase_whenFailedFetchingCart() {
        // given
        let expectation = self.expectation(description: "Fetch cart to fail")
        expectation.expectedFulfillmentCount = 1
        let cartRepository = CartRepositoryMock(fetchResult: .failure(CartRepositorySuccessTestError.failedFetching),
                                                saveResult: .success(()),
                                                clearResult: .success([]))
        let useCase = DefaultFetchCartUseCase(cartRepository: cartRepository)

        // when
        var responseValue: [Item]?
        useCase.execute(completion: { result in
            switch result {
            case .success(let value):
                responseValue = value
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(responseValue)
    }
}
