//
//  AuthenticationUseCaseTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/26/21.
//

import XCTest
@testable import Chocorian

class AuthenticationUseCaseTests: XCTestCase {
    
    static let authentication: Authentication = Authentication(token: "1234567890")
    var task: Cancellable?
    
    func testAuthenticationUseCase_whenSuccessfullyLogin() {
        // given
        let expectation = self.expectation(description: "Have a successfull login")
        expectation.expectedFulfillmentCount = 1
        let authenticationRepository = AuthenticationRepositoryMock(result: .success(Self.authentication))
        let useCase = DefaultAuthenticationUseCase(authenticationRepository: authenticationRepository)

        // when
        let requestValue = AuthenticationUseCaseRequestValue(email: "email@email.com", password: "password")
        var responseValue: Authentication?
        task = useCase.execute(requestValue: requestValue) { result in
            switch result {
            case .success(let value):
                responseValue = value
            default: break
            }
            expectation.fulfill()
        }
        
        // then
        waitForExpectations(timeout: 5.0, handler: nil)
        XCTAssertNotNil(responseValue)
        XCTAssertEqual(responseValue?.token, Self.authentication.token)
    }
    
    func testAuthenticationUseCase_whenFailedLogin() {
        // given
        let expectation = self.expectation(description: "Have the login to fail")
        expectation.expectedFulfillmentCount = 1
        let authenticationRepository = AuthenticationRepositoryMock(result: .failure(AuthenticationRepositorySuccessTestError.failedLogin))
        let useCase = DefaultAuthenticationUseCase(authenticationRepository: authenticationRepository)

        // when
        let requestValue = AuthenticationUseCaseRequestValue(email: "email@email.com", password: "password")
        var responseValue: Authentication?
        task = useCase.execute(requestValue: requestValue) { result in
            switch result {
            case .success(let value):
                responseValue = value
            default: break
            }
            expectation.fulfill()
        }
        
        // then
        waitForExpectations(timeout: 5.0, handler: nil)
        XCTAssertNil(responseValue)
        XCTAssertNotEqual(responseValue?.token, Self.authentication.token)
    }
}
