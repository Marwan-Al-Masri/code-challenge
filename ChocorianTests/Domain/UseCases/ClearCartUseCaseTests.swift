//
//  ClearCartUseCaseTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

class ClearCartUseCaseTests: XCTestCase {
    
    static let cart: [Item] = {
        Item.stub()
    }()
    
    func testClearCartUseCase_whenSuccessfullyClearCart() {
        // given
        let expectation = self.expectation(description: "Clear cart successfully")
        let cartRepository = CartRepositoryMock(fetchResult: .success([]),
                                                saveResult: .success(()),
                                                clearResult: .success(Self.cart))
        let useCase = DefaultClearCartUseCase(cartRepository: cartRepository)

        // when
        var responseValue: [Item]?
        useCase.execute(completion: { result in
            switch result {
            case .success(let value):
                responseValue = value
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(responseValue)
        XCTAssertEqual(responseValue?.count, Self.cart.count)
    }
    
    func testClearCartUseCase_whenFailedClearingCart() {
        // given
        let expectation = self.expectation(description: "Clear cart to fail")
        expectation.expectedFulfillmentCount = 1
        let cartRepository = CartRepositoryMock(fetchResult: .success([]),
                                                saveResult: .success(()),
                                                clearResult: .failure(CartRepositorySuccessTestError.failedClearing))
        let useCase = DefaultClearCartUseCase(cartRepository: cartRepository)

        // when
        var responseValue: [Item]?
        useCase.execute(completion: { result in
            switch result {
            case .success(let value):
                responseValue = value
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNil(responseValue)
    }
}
