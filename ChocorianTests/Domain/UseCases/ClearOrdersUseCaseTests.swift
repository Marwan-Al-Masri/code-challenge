//
//  ClearOrdersUseCaseTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

class ClearOrdersUseCaseTests: XCTestCase {
    
    static let orders: [Order] = {
        Order.stub()
    }()
    
    func testClearOrdersUseCase_whenSuccessfullyClearOrders() {
        // given
        let expectation = self.expectation(description: "Clear orders successfully")
        let orderRepository = OrderRepositoryMock(fetchResult: .success([]),
                                                  saveResult: .success(()),
                                                  clearResult: .success(()))
        let useCase = DefaultClearOrdersUseCase(orderRepository: orderRepository)

        // when
        useCase.execute(completion: { _ in
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testClearOrdersUseCase_whenFailedClearingOrders() {
        // given
        let expectation = self.expectation(description: "Clear orders to fail")
        expectation.expectedFulfillmentCount = 1
        let orderRepository = OrderRepositoryMock(fetchResult: .success([]),
                                                  saveResult: .success(()),
                                                  clearResult: .failure(OrderRepositorySuccessTestError.failedClearing))
        let useCase = DefaultClearOrdersUseCase(orderRepository: orderRepository)

        // when
        useCase.execute(completion: { _ in
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
    }
}
