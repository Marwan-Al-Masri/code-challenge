//
//  SaveCartUseCaseTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

class SaveCartUseCaseTests: XCTestCase {
    
    static let cart: [Item] = {
        Item.stub()
    }()
    
    func testSaveCartUseCase_whenSuccessfullySaveCart() {
        // given
        let expectation = self.expectation(description: "Save cart successfully")
        expectation.expectedFulfillmentCount = 1
        let cartRepository = CartRepositoryMock(fetchResult: .success([]),
                                                saveResult: .success(()),
                                                clearResult: .success([]))
        let useCase = DefaultSaveCartUseCase(cartRepository: cartRepository)

        // when
        let requestValue = Item.stub()
        var isSuccess = false
        useCase.execute(cart: requestValue, completion: { result in
            switch result {
            case .success:
                isSuccess = true
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertTrue(isSuccess)
    }
    
    func testSaveCartUseCase_whenFailedSavingCart() {
        // given
        let expectation = self.expectation(description: "Save cart to fail")
        expectation.expectedFulfillmentCount = 1
        let cartRepository = CartRepositoryMock(fetchResult: .success([]),
                                                saveResult: .failure(CartRepositorySuccessTestError.failedSaving),
                                                clearResult: .success([]))
        let useCase = DefaultSaveCartUseCase(cartRepository: cartRepository)

        // when
        let requestValue = Item.stub()
        var isSuccess = false
        useCase.execute(cart: requestValue, completion: { result in
            switch result {
            case .success:
                isSuccess = true
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertFalse(isSuccess)
    }
}
