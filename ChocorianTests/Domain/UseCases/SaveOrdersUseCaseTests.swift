//
//  SaveOrdersUseCaseTests.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

class SaveOrdersUseCaseTests: XCTestCase {
    
    static let orders: [Order] = {
        Order.stub()
    }()
    
    func testSaveOrdersUseCase_whenSuccessfullySaveOrders() {
        // given
        let expectation = self.expectation(description: "Save orders successfully")
        expectation.expectedFulfillmentCount = 1
        let ordersRepository = OrderRepositoryMock(fetchResult: .success([]),
                                                   saveResult: .success(()),
                                                   clearResult: .success(()))
        let useCase = DefaultSaveOrderUseCase(orderRepository: ordersRepository)

        // when
        let requestValue = Self.orders[0]
        var isSuccess = false
        useCase.execute(order: requestValue, completion: { result in
            switch result {
            case .success:
                isSuccess = true
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertTrue(isSuccess)
    }
    
    func testSaveOrdersUseCase_whenFailedSavingOrders() {
        // given
        let expectation = self.expectation(description: "Save orders to fail")
        expectation.expectedFulfillmentCount = 1
        let ordersRepository = OrderRepositoryMock(fetchResult: .success([]),
                                                   saveResult: .failure(OrderRepositorySuccessTestError.failedSaving),
                                                   clearResult: .success(()))
        let useCase = DefaultSaveOrderUseCase(orderRepository: ordersRepository)

        // when
        let requestValue = Self.orders[0]
        var isSuccess = false
        useCase.execute(order: requestValue, completion: { result in
            switch result {
            case .success:
                isSuccess = true
            default: break
            }
            expectation.fulfill()
        })
        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertFalse(isSuccess)
    }
}
