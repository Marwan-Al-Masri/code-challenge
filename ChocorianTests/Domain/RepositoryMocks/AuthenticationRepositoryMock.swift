//
//  AuthenticationRepositoryMock.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

enum AuthenticationRepositorySuccessTestError: Error {
    case failedLogin
}

struct AuthenticationRepositoryMock: AuthenticationRepository {
    var result: Result<Authentication, Error>
    func login(email: String, password: String,
               completion: @escaping (Result<Authentication, Error>) -> Void) -> Cancellable? {
        completion(result)
        return nil
    }
}
