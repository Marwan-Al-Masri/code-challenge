//
//  CartRepositoryMock.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

enum CartRepositorySuccessTestError: Error {
    case failedFetching
    case failedSaving
    case failedClearing
}

struct CartRepositoryMock: CartRepository {
    var fetchResult: Result<[Item], Error>
    func fetch(completion: @escaping (Result<[Item], Error>) -> Void) {
        completion(fetchResult)
    }
    var saveResult: Result<Void, Error>
    func save(cart: [Item], completion: @escaping (Result<Void, Error>) -> Void) {
        completion(saveResult)
    }
    var clearResult: Result<[Item], Error>
    func clear(completion: @escaping (Result<[Item], Error>) -> Void) {
        completion(clearResult)
    }
}
