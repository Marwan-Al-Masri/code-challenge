//
//  ProductRepositoryMock.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

enum ProductsRepositorySuccessTestError: Error {
    case failedFetching
}

struct ProductRepositoryMock: ProductRepository {
    
    var result: Result<[Product], Error>
    func fetchProcudts(token: String, completion: @escaping (Result<[Product], Error>) -> Void) -> Cancellable? {
        completion(result)
        return nil
    }
}
