//
//  OrderRepositoryMock.swift
//  ChocorianTests
//
//  Created by Marwan Al Masri on 6/27/21.
//

import XCTest
@testable import Chocorian

enum OrderRepositorySuccessTestError: Error {
    case failedFetching
    case failedSaving
    case failedClearing
}

struct OrderRepositoryMock: OrderRepository {
    var fetchResult: Result<[Order], Error>
    func fetch(completion: @escaping (Result<[Order], Error>) -> Void) {
        completion(fetchResult)
    }
    var saveResult: Result<Void, Error>
    func save(order: Order, completion: @escaping (Result<Void, Error>) -> Void) {
        completion(saveResult)
    }
    var clearResult: Result<Void, Error>
    func clear(completion: @escaping (Result<Void, Error>) -> Void) {
        completion(clearResult)
    }
}
